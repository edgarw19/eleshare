
// service 
app.factory('users', ['$http', 'auth', function($http, auth) {

    var o = {};

    o.getUser = function(id,cb) {
          return $http.get('/users/' + id, {headers: {Authorization: 'Bearer ' + auth.getToken()}});
              

    };

    o.getSearch = function(searchTerm, cb) {
        return $http.get('/users/search/' + searchTerm).success(function(data){ 
            //console.log(data);
            cb(data); 
        });
    };
    o.getCategories = function(id, cb){

      return $http.get('/users/' + id + '/categories', {headers: {Authorization: 'Bearer ' + auth.getToken()}});

      // .success(function(data)
      //  {
        
      //    cb(data); 
      //    //return data;
      //  });
    };

    o.getNotifications = function(id, cb) {
        return $http.get('/users/' + id + '/notifications').success(function(data) {
            cb(data); 
        }); 
    }; 

    o.editEmail = function(id,newEmail){
      // console.log(newEmail);
      // console.log(id);
        return $http.post('/users/' + id +  '/editEmail', newEmail,
         {headers: {Authorization: 'Bearer ' + auth.getToken()}
    });
    };

    o.addCategories = function(id, categories){
        // console.log("I am here");
        return $http.post('/users/' + id +  '/addCategories', categories,
         {headers: {Authorization: 'Bearer ' + auth.getToken()}
    });
    };

    o.addFollows = function(userToAdd, authUser){
        //currentUser = auth.getCurrentUser();
        return $http.put('/users/'+authUser._id+'/addFollows/'+userToAdd.data._id,
        {headers: {Authorization: 'Bearer ' + auth.getToken()}
        })
            .success(function(data){
                authUser.follows.push(userToAdd.data._id);
                userToAdd.data.followers.push(authUser._id);
            });
     };

     o.getUserCB = function(id) {
        return $http.get('/users/' + id);
    };

   

    o.deleteCategories = function(id, categories){
        // console.log("I am here");
        return $http.post('/users/' + id +  '/deleteCategories', categories,
         {headers: {Authorization: 'Bearer ' + auth.getToken()}
      });
    };

    o.addSavedPost = function(authUser, post){
      return $http.put('/posts/' + post._id +'/'+ authUser._id +'/save', null, {
        headers: {Authorization: 'Bearer ' + auth.getToken()}
      })
        .success(function(data){
          console.log("SUCCESS!");
            authUser.savedPosts.push(post._id);
        });
    };

    

    return o; 

}]);
