
app.service('rssFeed', function($q, $rootScope) {
        this.get = function(url) {
            var d = $q.defer();
            var feed = new google.feeds.Feed(url);
            feed.setNumEntries(10);
            feed.load(function(result) {
              // console.log("result:" + Object.keys(result));
              // console.log("result1:" + Object.keys(result.feed));
                $rootScope.$apply(d.resolve(result));
            });
            return d.promise;
        }
    });

app.factory('feedFactory',['$http', 'auth', function($http, auth){
    var o = {};

    o.addFeed = function(id, feedLink, category){
      // console.log(id);
      // console.log(feedLink);
      // console.log(category);
      var feedLink = feedLink.split("");
      var feedObj = {Link: feedLink};
      // console.log(feedObj);
      return $http.post('/users/' + id + '/addFeed',feedObj, {headers: {Authorization: 'Bearer ' + auth.getToken()}});
    };

     o.removeFeed = function(userID, url){
      console.log("I am here");
      // console.log(id);
      // console.log(feedLink);
      // console.log(category);
      //var feedLink = feedLink.split("");
      var feedObj = {link: url};
      //console.log(feedObj);
      return $http.post('/users/' + userID + '/removeFeed',feedObj, {headers: {Authorization: 'Bearer ' + auth.getToken()}});
    };
    return o;
}]);