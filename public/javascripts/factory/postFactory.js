var dateToDay = ["Sun", "Mon", "Tues", "Wed", "Thurs", "Fri", "Sat"];
var dateToString = function(date){
  var dateMinutes = "";
      if (date.getMinutes() < 10){
        dateMinutes+="0";
      }
      dateMinutes+=date.getMinutes();
      var timeString = dateToDay[date.getDay()] + " ";
      if (date.getHours() >= 12){
        timeString += (date.getHours()-12) + ":" + dateMinutes +"PM";
      }
      else {
        timeString+=date.getHours()+":"+dateMinutes+"AM";
      }
    return timeString;
}

// service 
app.factory('posts', ['$http', 'auth', 'users', function($http, auth, users) {

  var o = {
    posts: []
  };

  o.getAll = function() {
    id = auth.fullUser()._id; 
    var maxChars = 300; 

    // get user categories
    if (id){
    return $http.get('/users/' + id + '/categories')
     .success(function(data) {
      categoriesStr = ""; 

      for (i in data) {
        categoriesStr += data[i];
        if (i < data.length - 1) categoriesStr += '|'; 
      }
      if(true)
      // return $http.get('/posts/category/' + categoriesStr).success(function(data){
        return $http.get('/posts').success(function(data){
        angular.copy(data, o.posts);

        var allScrapedData = {};    // indexed by post id 
        var completedRequests = 0; 
        // get scraped data for each post
        for (var i = 0; i < o.posts.length; i++) {
            o.posts[i].scrapedData.text = o.posts[i].scrapedData.text.substring(0, maxChars);

            // get updated scraped data 
            $http.get('/posts/' + o.posts[i]._id + '/getScrapedData').success(function(data) {

                result = {
                    title : data.title,
                    text : data.text.substring(0, maxChars) + " ...",
                    image : data.image
                }; 

                allScrapedData[data.id] = result; 

                completedRequests++; 
                // all requests for scraped data completed
                // we do this bc http req are asynchronous 
                if (completedRequests == o.posts.length) { 
                    for (var j = 0; j < o.posts.length; j++) {
                        o.posts[j].scrapedData = allScrapedData[o.posts[j]._id];
                    }
                }
                     
            });


            }

          });
      
        });
      }

  };

    o.getUserPosts = function(user_id, cb) {
        return $http.get('/posts/getUserPosts/' + user_id).success(function(data){
            cb(data); 
        }); 
    }

    o.getSavedPosts = function(user_id, cb) {
      return $http.get('/posts/getSavedPosts/' + user_id).success(function(data){
            cb(data); 
        }); 
    }

  o.getSearch = function(searchTerm, cb) {
    return $http.get('/posts/search/' + searchTerm).success(function(data){ 
        cb(data); 
    });
  }

  o.getComments = function(post, cb) {
    return $http.get('/posts/' + post._id + '/getComments').success(function(data) {
        cb(data); 
    });
  }

  o.create = function(post) {
    return $http.post('/posts', post, {
      headers: {Authorization: 'Bearer ' + auth.getToken()}
    }).success(function(data){
      console.log(data);
      console.log("WTF MANa");
      o.posts.push(data);
    }).error(function(err){
      console.log(err);
    });
  };

  // o.getRelated = function(authorID, cb){
  //   return $http.get('/users/' + authorID + '/recent')
  //   .success(function(data){
  //     cb(data);
  //   })
  //   .error(function(err){

  //   });
  // };

  o.upvote = function(post) {
    username = auth.currentUserID();
    if(post.upvoterIDs.indexOf(username) == -1){
      return $http.put('/posts/' + post._id + '/upvote', null, {
        headers: {Authorization: 'Bearer ' + auth.getToken()}
      })
        .success(function(data){
         post.upvotes += 1;
         post.upvoterIDs.push(username);
        });
      };
  };

  o.downvote = function(post) {
    username = auth.currentUserID();
    var index = post.upvoterIDs.indexOf(username);
    if (index > -1){
      post.upvoterIDs.splice(index, 1);
      post.upvotes -= 1;
      return $http.put('/posts/' + post._id + '/downvote', null, {
        headers: {Authorization: 'Bearer ' + auth.getToken()}
      }).success(function(data){

      }).error(function(err){

      });
    }
  }
    
  o.get = function(id) {
    return $http.get('/posts/' + id).then(function(res){
      return res.data;
    });
  };

  o.addComment = function(id, comment) {
    return $http.post('/posts/' + id + '/comments', comment, {
      headers: {Authorization: 'Bearer ' + auth.getToken()}
    });
  };

  o.upvoteComment = function(post, comment) {
    username = auth.currentUserID();
    console.log(comment);
    console.log("TRYING TO UPVOTE DUDE");
    if(comment.upvoterIDs.indexOf(username) == -1)
    {
      console.log("DID UPVOTE MAN??");
      return $http.put('/posts/' + post._id + '/comments/'+ comment._id + '/upvote', null, 
        {headers: {Authorization: 'Bearer ' + auth.getToken()}
    })
      .success(function(data){
        comment.upvoterIDs.push(username);
        comment.upvotes += 1;
        comment.isUpvoted = true;
      });
    }
  };

  o.downvoteComment = function(post, comment) {
    userID = auth.currentUserID();
    var index = comment.upvoterIDs.indexOf(userID);
    if (index > -1){
      comment.isUpvoted = false;
      comment.upvotes -= 1;
      comment.upvoterIDs.splice(userID, 1);
    }
    return $http.put('/posts/' + post._id + '/comments/' + comment._id + '/downvote', null, 
      {headers: {Authorization: 'Bearer ' + auth.getToken()}}
    ).success(function(data){

    }).error(function(err){

    });

  };

  o.deleteComment = function(post, comment){
    console.log(post);
    for (var i = 0; i < post.comments.length; i++){
      if (post.comments[i]._id === comment._id){
        post.comments.splice(i, 1);
        break;
      }
    }
    return $http.delete('/posts/'+post._id + '/comments/' + comment._id +'/delete',
       {headers: {Authorization: 'Bearer ' + auth.getToken()}}
      ).success(function(data){

      }).error(function(err){

      });
  }

  o.deletePost = function(post){
    return $http.delete('/posts/'+post._id+'/delete', {
      headers: {Authorization: 'Bearer '+auth.getToken()}
    });
  };
 
  return o; 

}]);

