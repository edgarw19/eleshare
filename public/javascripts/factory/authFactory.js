app.factory('auth', ['$q', '$http', '$window', function($q, $http, $window){
  var auth = {};

  auth.saveToken = function (token){
    $window.localStorage['flapper-news-token'] = token;
  };

  auth.getToken = function (){
    return $window.localStorage['flapper-news-token'];
  };

  auth.isLoggedIn = function(){
    var token = auth.getToken();
    if(token){
      // console.log("is logged in dude");
        // decode encoding 
        var payload = JSON.parse($window.atob(token.split('.')[1]));
        return payload.exp > Date.now() / 1000;
      } 
      else {
        // console.log("isn't logged in");
        return false;
      }
    };
  auth.currentUser = function(){
    if (auth.isLoggedIn()){   
      return $q(function(resolve, reject){
        auth.getCurrentUser = auth.fullUser();
        resolve(auth.getToken());
      });
    }
    else {
      return $http.get('/curUser/').
      success(function(res){
        auth.getCurrentUser = res;
        auth.logIn(res);
        return res;
      }).error(function(err){
        return;
      });
    }
  };

  auth.fullUser = function(){
    if (auth.isLoggedIn()){
      var token = auth.getToken();
      var payload = JSON.parse($window.atob(token.split('.')[1]));
      return payload;
    }
  }



  auth.currentUserID = function(){
    if (auth.isLoggedIn()){
      var token = auth.getToken();
      var payload = JSON.parse($window.atob(token.split('.')[1]));
      return payload._id;
    }


  };

  auth.register = function(user){
    return $http.post('/register', user).success(function(data){
      auth.saveToken(data.token);
    });
  };

  auth.logIn = function(user){
    return $http.post('/generateToken', user).success(function(data){
      auth.saveToken(data.token);
    });
  };

  auth.logOut = function(){
    $window.localStorage.removeItem('flapper-news-token');
  };

  return auth;

}])

