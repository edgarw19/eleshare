app.config([
    '$stateProvider',
    '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('home', {
                url: '/home',
                templateUrl: '/home.html',
                controller: 'MainCtrl',
                resolve: {
                    postPromise: ['posts', 'auth', function(posts, auth){
                      return auth.currentUser().then(function(){
                        return posts.getAll();
                      });
                        
                    }],
                    
                }
            })

            .state('posts', {
                url: '/posts/{id}',
                templateUrl: '/templates/posts.ejs',
                controller: 'PostsCtrl',
                resolve: {
                  post: ['$stateParams', 'posts', function($stateParams, posts) {
                    return posts.get($stateParams.id);
                  }]
                }
            })

         
      


             .state('settings', {
              url: '/settings',
              templateUrl: '/settings.html',
              controller: 'SettingsCtrl',
              resolve:  {
                  user: ['$stateParams', 'users', 'auth', function($stateParams, users, auth) {
                    return users.getUser(auth.currentUserID());
                  }],
                  category: ['$stateParams', 'users', 'auth', function($stateParams, users, auth){
                   
                }],
                feedList: ['$stateParams', 'users', 'auth', function($stateParams, users, auth){
                    return users.getUser(auth.currentUserID());
                }]
                }
            })

             .state('subscribedSites', {
              url: '/subscribedSites',
              templateUrl: '/subscribedSites.html',
              controller: 'RssFeederCtrl',
              resolve:{ 
                user: ['$stateParams', 'users', 'auth', function($stateParams, users, auth) {
                    return users.getUser(auth.currentUserID());
                  }],
                feedList: ['$stateParams', 'users', 'auth', function($stateParams, users, auth){
                    return users.getUser(auth.currentUserID());
                }]
                }   
            })

           
            .state('users', {
                url: '/users/{username}',
                templateUrl: '/users.html',
                controller: 'UserProfileCtrl',
                resolve: {
                    userProfilePictureLink:['$stateParams', 'users', 'auth', function($stateParams, users, auth) {
                        console.log("I am here in users.");
                        var url = "";

                     }
                    ],
                    user: ['$stateParams', 'users', 'auth', function($stateParams, users, auth) {
                    return users.getUser(auth.currentUserID());
                    }],
                    userToFollow: ['$stateParams', 'users', 'auth', function($stateParams, users, auth) {
                        return users.getUser($stateParams.username);
                    }],
                    userPosts: ['$stateParams', 'posts', 'users', function($stateParams, posts, users){
                        return posts.getUserPosts($stateParams.username, function(posts) {
                            if (posts) return posts; 
                            else return []; 
                        }); 
                    }],
                    savedPosts: ['$stateParams', 'posts', 'users', function($stateParams, posts, users){
                        return posts.getSavedPosts($stateParams.username, function(savedPosts) {
                            if (savedPosts) return savedPosts; 
                            else return []; 
                        }); 
                    }],
                    isMyPage: ['$stateParams', 'auth', function($stateParams, auth){
                        return $stateParams.username == auth.getCurrentUser._id;
                    }],

                    isFollowing: ['$stateParams', 'auth', 'users', '$q', function($stateParams, auth, users, $q){
                        return users.getUserCB(auth.currentUserID()).success(function(userA){
                            return $q(function(resolve, reject){
                                if (userA.follows.indexOf($stateParams.username) == -1){
                                    resolve(false);
                                }
                                else {
                                    resolve(true);
                                }
                            }); 
                        })
                    }],
                }
            })

            .state('search', {
                url: '/search/{searchTerm}',
                templateUrl: '/search.html',
                controller: 'SearchCtrl', //TODO: make a search controller?
                resolve: {
                    // isLoggedIn: ['posts', function($stateParams, auth) {
                    //     return auth.isLoggedIn(); 
                    // }]
                    
                    // searchResults: [function() { return []; }]
                    // postPromise: ['posts', function(posts) {
                    //     return [];
                    //     // return posts.getSearch("corgi"); //TODO: change this 
                    // }]
                    foundUsers:['$stateParams', 'posts', 'users', function($stateParams, posts, users){
                        console.log("I am here: " + $stateParams.searchTerm);
                        return users.getSearch($stateParams.searchTerm, function(newUsers) {
                            console.log(newUsers);
                            return newUsers; 
      
                    });
                    }],
                    foundPosts:['$stateParams', 'posts', 'users','$http', function($stateParams, posts, users, $http){
                        return posts.getSearch($stateParams.searchTerm, function(newPosts) {
                       
                        console.log( newPosts);
                        var allScrapedData = {};    // indexed by post id 
                        var completedRequests = 0; 
                        // get scraped data for each post
                        for (var i = 0; i < newPosts.length; i++) {
                        // console.log(i, o.posts[i].title); 

                        // var id = o.posts[i]._id; 
                        // console.log(id); 
                        $http.get('/posts/' + newPosts[i]._id + '/getScrapedData').success(function(data) {

                            result = {
                                title : data.title,
                                text : data.text.substring(0, 300) + " ...",
                                image : data.image
                            }; 

                            allScrapedData[data.id] = result; 

                            completedRequests++; 
                            // all requests for scraped data completed
                            // we do this bc http req are asynchronous 
                            if (completedRequests == newPosts.length) { 
                                for (var j = 0; j < newPosts.length; j++) {
                                    newPosts[j].scrapedData = allScrapedData[newPosts[j]._id];
                                }
                            }
                           
                    
                });
         
            }
            
            return newPosts;
        });
      
        }]         
    }
})

        $urlRouterProvider.otherwise('home');
}]);

