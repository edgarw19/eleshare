app.controller('NotificationsCtrl', [
    '$scope',
    'auth', '$http', 'users',
    function($scope, auth, $http, users){
        users.getNotifications(auth.currentUserID(), function(notifications) {
        // auth.currentUser().then(function(fullUser){
            // users.getNotifications(fullUser._id, function(notifications) {
            if (notifications.length == 0) {
                $scope.userNotifications = [{
                    text: "no notifications!"
                }]
                $scope.numUnread = 0;
            } else {
                $scope.userNotifications = notifications; 

                $scope.numUnread = 0; 
                for (var i = 0; i < notifications.length; i++) {
                    if (!notifications[i].isRead) $scope.numUnread++; 

                    if (notifications[i].notification_type == 'comment' || notifications[i].notification_type == 'upvote' || notifications[i].notification_type == 'comment upvote') {
                        $scope.userNotifications[i].text = '<b>'; 

                        // TODO: only show the first 2-3 users 
                        for (var j = 0; j < notifications[i].user_names.length; j++) {
                            $scope.userNotifications[i].text += notifications[i].user_names[j];
                            if (j < notifications[i].user_names.length - 1) $scope.userNotifications[i].text += ', '
                        }

                        $scope.userNotifications[i].text += "</b>";

                        if (notifications[i].notification_type == 'comment') {
                            $scope.userNotifications[i].text += ' commented on your post <b>\''
                                            + notifications[i].post_title + '\'</b>'; 
                        } else if (notifications[i].notification_type == 'upvote') {
                            $scope.userNotifications[i].text += ' upvoted your post <b>\''
                                            + notifications[i].post_title + '\'<b>'; 
                        } else if (notifications[i].notification_type == 'comment upvote') {
                            $scope.userNotifications[i].text += ' upvoted your comment <b>\'' 
                                            + notifications[i].comment_body + '\'<b>'; 
                        }

                        $scope.userNotifications[i].link = 'newsfeed#/posts/' + $scope.userNotifications[i].post_id; 
                    } else if (notifications[i].notification_type == 'follower') {
                        $scope.userNotifications[i].text = '<b>' + notifications[i].user_names[0] + '</b> followed you!';
                        $scope.userNotifications[i].link = 'newsfeed#/users/' + $scope.userNotifications[i].user_id; 
                    }


                }

                $scope.userNotifications.sort(function(a, b) {
                    return a.timeMillis < b.timeMillis; 
                })
            }
        });
        // });
        
        $scope.updateStatuses = function() {
            console.log("notification center opened"); 
            $scope.numUnread = 0; 
        }

}]);
