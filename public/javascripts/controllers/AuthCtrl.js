app.controller('AuthCtrl', [
    '$scope',
    '$state',
    'auth',
    function($scope, $state, auth){
        $scope.user = {};

        $scope.categories = [
          'Architecture', 
          'Art', 
           'Business',
           'Education',
           'Entertainment',
           'Fashion',
           'Funny',
           'Health',
           'History',
           'Interesting',
           'Life Thoughts',
           'News',
           'Philosophy',
           'Politics',
           'Science',
           'Sports',
           'Technology',
           'Travel'
        ]
 
        $scope.register = function(){
            console.log("AuthCtrl user: ", $scope.user); 
            auth.register($scope.user).error(function(error){
                $scope.error = error;
            }).then(function(){
                $state.go('home');
            });
        };

        $scope.logIn = function(){
            auth.logIn($scope.user).error(function(error){
                $scope.error = error;
            }).then(function(){
                $state.go('home');
            });
        };
    }
]);

