app.controller('initialLandingPageCtrl', [
    '$scope', '$http','rssFeed','feedFactory',
    'users',
    'auth', '$window', 'LxDialogService', 'LxNotificationService',
    function($scope, $http, rssFeed, feedFactory, 
     users, auth, $window, LxDialogService, LxNotificationService) {
         //document.domain = "localhost:3000";
    //console.log("I am here in InitialLandingPageCtrl");
    //$scope.userName = user.username;
    $scope.currentUserDetails = auth.currentUser().then(function(data){
    // console.log(Object.keys(data));

    // console.log(Object.keys(data.data));
    // console.log(data.data);
    // console.log(auth.getCurrentUser);

    $scope.currentUser = auth.getCurrentUser;
 
   
     
     
      users.getCategories(data.data._id).success(function(data) {
        //console.log(data);
        $scope.categories = data; 

      });

       $scope.feedList = [];
    for(i = 0; i < $scope.currentUser.feeds.length; i++)
    {
        $scope.feedList.push(feedList.data.feeds[i]);
    }
      // users.getUser(auth.fullUser()._id).success(function(user) {
      //       console.log("I am here");
      //       $scope.user = user;
      //       $scope.userEmail = user.email;
      //        $scope.authID = auth.currentUserID();
        
      //   $scope.fullUser = auth.fullUser;
      //   });

    });

        $scope.isLoggedIn = auth.isLoggedIn; 

   
$scope.rssFeedsChoice = [
        '//feeds.nytimes.com/nyt/rss/HomePage',
        'http://www.washingtonpost.com/rss/',

        'http://rssfeeds.usatoday.com/usatoday-NewsTopStories',
        'http://www.npr.org/rss/rss.php?id=1001',
        'http://feeds.reuters.com/reuters/topNews',
        'http://newsrss.bbc.co.uk/rss/newsonline_world_edition/americas/rss.xml'
        ,'http://feeds.sciencedaily.com/sciencedaily',
        'http://www.nasa.gov/rss/image_of_the_day.rss',
        'http://feeds.bbci.co.uk/news/rss.xml',
        'http://feeds.bbci.co.uk/news/business/rss.xml',
        'http://feeds.bbci.co.uk/news/health/rss.xml',
        'http://feeds.bbci.co.uk/news/entertainment_and_arts/rss.xml',
        'http://rss.cnn.com/rss/money_latest.rss',
        'http://rss.cnn.com/rss/cnn_us.rss',
        'http://rss.cnn.com/rss/cnn_latest.rss',
        'http://rss.cnn.com/rss/cnn_topstories.rss',
        'http://feeds.feedburner.com/TechCrunch/startups',
        'http://feeds.feedburner.com/Mobilecrunch',
        'http://feeds.feedburner.com/TechCrunch/social',
        'http://feeds.feedburner.com/TechCrunchIT',
        'http://feeds.feedburner.com/TechCrunch/',
        'http://rss.cnn.com/rss/si_topstories.rss',
        'http://rss.nytimes.com/services/xml/rss/nyt/Sports.xml',
        'http://sports.yahoo.com/top/rss.xml',
        'http://www.nba.com/jazz/rss.xml',
        'http://www.npr.org/rss/rss.php?id=1008',
        'http://www.newyorker.com/feed/humor',
        'http://www.npr.org/rss/rss.php?id=13',
        'http://www.npr.org/rss/rss.php?id=1045',
        'http://www.nationalgeographic.com/adventure/nga.xml', 'http://sports.espn.go.com/espn/rss/news',
        'http://sports.espn.go.com/espn/rss/nfl/news',

        'http://sports.espn.go.com/espn/rss/nba/news',

        'http://sports.espn.go.com/espn/rss/mlb/news',

        'http://sports.espn.go.com/espn/rss/nhl/news',

        'http://sports.espn.go.com/espn/rss/rpm/news',

        'http://soccernet.espn.go.com/rss/news',

        'http://sports.espn.go.com/espn/rss/espnu/news',

        'http://sports.espn.go.com/espn/rss/ncb/news',

        'http://sports.espn.go.com/espn/rss/ncf/news'
        ]
        
        //add links without hyphens and also try to use underscore for spaces
         $scope.rssFeedsChoiceNoHttp = [
        'New York Times Home Page',
        'Washington Post',
        'USATODAY.com News Top Stories',
        'NPR Topics_News',
        'Reuters_Top News',
        'BBC News_Americas_World Edition',
        'ScienceDaily Headlines',
        'NASA Image of the Day',
        'bbc_news',
        'bbc_news_business',
        'bbc_news_health',
        'bbc_news_entertainment_and_arts',
        'cnn_money_latest',
        'cnn_us',
        'cnn_latest',
        'cnn_topstories',
        'TechCrunch_startups',
        'TechCrunch_Mobilecrunch',
        'TechCrunch_social',
        'TechCrunch_IT',
        'TechCrunch_latest',
        'Sports Illustrated',
        'New York Times_Sports',
        'Yahoo_Sports_Top News',
        'NBA.com_Jazz News',
        'NPR Topics - Arts & Culture',
        'New Yorker Humor',
        'NPR Fresh Air with Terry Gross',
        'NPR Topics: Movies',
        'National Geographic Adventure', 'ESPN_news',
        'ESPN_NFL Headlines',
        'ESPN_NBA Headlines',
        'ESPN_MLB Headlines',
        'ESPN_NHL Headlines',
        'ESPN_Motorsports Headlines',
        'ESPN_Soccer Headlines',
        'ESPN_ESPNU',
        'ESPN_College Basketball Headlines',
        'ESPN_College Football Headlines'
        ]

       
    
        $scope.pictureUrl = "";
        $scope.categoriesChoice = [
           'Architecture', 
           'Art', 
           'Business',
           'Education',
           'Entertainment',
           'Food',
           'Fashion',
           'Funny',
           'Health',
           'History',
           'Interesting',
           'Life Thoughts',
           'News',
           'Philosophy',
           'Politics',
           'Science',
           'Sports',
           'Technology',
           'Travel'
        ];

        $scope.loadItems = function(query){
            var regexp = new RegExp(query.toLowerCase());
            var matches = [];
      
            for (category in $scope.categoriesChoice){
                var obj = {};
                obj.text = $scope.categoriesChoice[category].toLowerCase();
                if (regexp.test(obj.text)){
                    matches.push(obj);
                }
            }
            return matches;
        };

        $scope.editEmail = function(newEmail){
            users.editEmail($scope.currentUser._id,newEmail).success(function(data) {
                $scope.userEmail= data.email; 
            });
            $scope.email = '';
        };

        $scope.getMyLastName = function() {
          //console.log("I am here");
          //console.log($scope.pictureUrl2);
          
          //console.log("I am here");
           
         };
      

        $scope.addNewCategories = function() {
            if (!$scope.tags) { return; }
            var categoriesArray = [];
            for (i in $scope.tags) {
              for (j= 0; j< $scope.categoriesChoice.length; j++)
              {
                console.log($scope.categoriesChoice[j].toLowerCase());
                var splitFeedUrl = $scope.tags[i].text.toLowerCase().split('-');
                var concacted = splitFeedUrl.join(" ");
                console.log(concacted);
                if(concacted == $scope.categoriesChoice[j].toLowerCase())
                  {
                    console.log("I am here");
                    categoriesArray.push($scope.tags[i].text.toLowerCase());
                  }
              }
            }
            $scope.tagLength =  $scope.tags.length;
            console.log(categoriesArray);
            users.addCategories(auth.fullUser()._id,categoriesArray).success(function(user) {
                for (i = 0; i < $scope.tagLength; i++) {     
                    var index = $scope.categories.indexOf(categoriesArray[i]);
                    if (index == -1 && categoriesArray.length != 0) {
                        $scope.categories[categoriesArray[i]]  = false;
                        $scope.categories.push(categoriesArray[i]);
                    }
                }
            });
            $scope.tags = '';
        };

        $scope.deleteCategories = function() {

            var categoriesArray = [];

            for (var category in $scope.categories) {
                $scope.newCategory = {
                    'category' :  $scope.categories[category],
                    'value' : $scope.categories[$scope.categories[category]]
                }
                categoriesArray.push($scope.newCategory);
                $scope.newCategory = {};
            }
        
            users.deleteCategories(auth.fullUser()._id,categoriesArray).success(function(user) {
                for(i = 0; i < categoriesArray.length; i++) {            
                    if (categoriesArray[i].value) {
                    var index = $scope.categories.indexOf(categoriesArray[i].category);
                    if (index != -1) {
                      $scope.categories.splice(index, 1);
                    }
                }
            }
        });
    };

    $scope.Done = function(){
      console.log("I am here");
      if($scope.feedList.length < 1)
      {
        $scope.openDialog('test2');
       }
      else
      {
        console.log("I am here");
        $window.location.href = '/newsfeed#/home';
      }
    }

    
    $scope.viewDetail = function(entry) {
      $scope.setCurrEntry(entry);
      $location.path('/detail');
    };
 $scope.loadItemsRss = function(query){
      var regexp = new RegExp(query.toLowerCase());
      var matches = [];
      // var queryBundles = query.split('.');
      // console.log(queryBundles[0]);
      // $http.jsonp('http://ajax.googleapis.com/ajax/services/search/web?v=2.0&q=' + queryBundles[0] + '%20' + '%20' + 'rss' + '&callback=hndlr');
      //  $scope.gResponse = gResponse;
      // console.log($scope.gResponse);
      // return $http.post('/getDetailedData',$scope.gResponse, {headers: {Authorization: 'Bearer ' + auth.getToken()}}).success(
      //   function(data){
      //     console.log("I am here");
      //     console.log(data);


      //     for(i =0; i < data.length; i++)
      //     {
      //       if(data[i]!= null)
      //       {
      //         if(((data[i].search('rss') != -1  &&  data[i].search('feed')!= -1) || data[i].search('feeds') != -1 || data[i].search('rss.xml')!= -1) && data[i].search('https')== -1) 
      //            {

      //               matches.push(data[i]);
      //            }
      //       }
      //     }


            var matches = [];

            for (cat in $scope.rssFeedsChoice){
                var obj = {};
                var obj2 = {};
                obj.text = $scope.rssFeedsChoice[cat].toLowerCase();
                obj2.text = $scope.rssFeedsChoiceNoHttp[cat].toLowerCase();
                if (regexp.test(obj.text)){
                  
                  matches.push(obj2);
                }
            }
            return matches;
          // newMatches = [];
      //   for(i =0; i < matches.length; i++)
      //   {
      //     var indices = [];
      //     var idx = matches.indexOf(matches[i]);
      //     while (idx != -1) {
      //         indices.push(idx);
      //         idx = matches.indexOf(matches[i], idx + 1);
      //     }
      //     console.log( i + " indices " + indices);
      //     if(indices.length != 0)
      //     newMatches.push(matches[indices[0]]);
      //   else
      //     newMatches.push(matches[indices]);
      //     for(j = 1; j < indices.length; j++)
      //     {
      //       matches.splice(indices[j],1);
      //     }
      //     console.log(matches.length);

      // } 
      // console.log(newMatches);

      // var len = matches.length;
      // if (len > 5)
      //   len = 5;
      // for(i = 0; i < len; i++)
      // {
      //   for (j = 0; j < len; j++)
      //     {
      //       console.log(matches[i]);
      //       if (matches[i] == matches[j])
      //       {
      //         matches.splice (j,1);
      //         len--;
      //     }
          
      //   }
      //   newMatches.push(matches[i]);
      // }
      
      // if(newMatches.length != 0)
      //  { console.log(newMatches);
      //   return newMatches;
      // }
          
        };
     $scope.addFeed = function() {
      //console.log("I am here
       //console.log("I am here
      var url;
      var http = "http://";
      // console.log("I am here");
      // console.log($scope.newFeedUrl);
      // console.log($scope.newFeedUrl[0].text);
      for (i = 0; i < $scope.rssFeedsChoiceNoHttp.length; i++)
          {
            $scope.rssFeedsChoiceNoHttp[i];
            console.log($scope.rssFeedsChoiceNoHttp[i]);
            var splitFeedUrl = $scope.newFeedUrl[0].text.split('-');
            console.log(splitFeedUrl);
            var concacted = splitFeedUrl.join(" ");
            console.log(concacted);
            if(concacted === $scope.rssFeedsChoiceNoHttp[i].toLowerCase()){
               // console.log($scope.newFeedUrl);
               // console.log("I am here");
              var url = $scope.rssFeedsChoice[i];
            }
             
        }
      //var url = $scope.newFeedUrl;
      //console.log(url);
      
      console.log(url);
      if(url)
      {
      if (url.indexOf(http) == -1) {
        url = http + url; // add http if missing
      }
      // console.log($scope.user);
      // console.log($scope.categoryID(category));

      // console.log(url + '&callback=?');
      // $http.get( url).success(function(data, status, headers, config) {
      //   $scope.htmlData = $sce.trustAsHtml(data);
      //   console.log("I am here in RssFeeds");
      //   console.log("data:" + data);
      //   $scope.status = status;
      //   console.log("status:" + status);
      // });
     //  console.log("I am about to write");
     // fs.writeFileSync('feedFolder/' + 'feed' + '.txt',body);
      //console.log($scope.feedList.length);
        if($scope.feedList.length < 5)
        {
          $scope.loadFeed(url, function() {
        
           feedFactory.addFeed($scope.currentUser._id, url)
           .success(function(feedList){
           
            $scope.feedList = [];
            for(i = 0; i < feedList.length; i++)
            {      
              $scope.feedList.push(feedList[i]);
            }
            $scope.feedListNoHttp = [];
                for(i = 0; i < $scope.feedList.length; i++)
      {
        for(j = 0; j < $scope.rssFeedsChoice.length;j++)
        {
          if($scope.feedList[i] == $scope.rssFeedsChoice[j])
          {
           
              $scope.feedListNoHttp.push($scope.rssFeedsChoiceNoHttp[j]);
            
          } 
        } 
      }
          });
       })
      }

     else {
          $scope.opendDialog('test');
       }
      }
      else{
        var url = $scope.newFeedUrl[0].text;
        console.log(url);
        if (url.indexOf(http) == -1) {
        url = http + url; // add http if missing
      }

          if($scope.feedList.length < 5)
        {
          $scope.loadFeed(url, function() {
          console.log("I am here");
           feedFactory.addFeed($scope.user._id, url)
           .success(function(feedList){
           console.log("I am here");
            $scope.feedList = [];
            for(i = 0; i < feedList.length; i++)
            {      
              $scope.feedList.push(feedList[i]);
            }
              $scope.feedListNoHttp = [];
      for(i = 0; i < $scope.feedList.length; i++)
      {
        for(j = 0; j < $scope.rssFeedsChoice.length;j++)
        {
          if($scope.feedList[i] == $scope.rssFeedsChoice[j])
          {
           
              $scope.feedListNoHttp.push($scope.rssFeedsChoiceNoHttp[j]);
            
          } 
        } 
      }
          });
       })
      }
      }
     $scope.newFeedUrl = '';
    };


  $scope.openDialog = function(dialogId)
    {
      //console.log("I am in opendDialog");
      LxDialogService.open(dialogId);
    };

    $scope.closingDialog = function()
    {
      LxNotificationService.info('Thank you!');
    };
    $scope.LessThanTen = function(){
      if($scope.feedList.length < 1)
      {
        return true;
      }
      if($scope.feedList.length >= 1)
      {
        return false;
      }
    }
    $scope.removeFeed = function(idx) {
      $scope.currentLoadedFeed = $scope.feedList[idx];
      //console.log("currentLoadedFeed:" + $scope.currentLoadedFeed);
      //console.log(idx);
      feedFactory.removeFeed($scope.currentUser._id,$scope.feedList[idx])
      .success(function(feedList){
        // console.log(feedList);
        // console.log($scope.feedList.length);
        if(feedList.length == 0 && $scope.feedList.length != 0)
        {

          $scope.currentLoadedFeed = $scope.feedList[idx];
          $scope.feedList = [];
        }
          else{
            $scope.feedList = [];
           for(i = 0; i < feedList.length; i++)
            {


              //console.log(feedList[i]);
              $scope.currentLoadedFeed = $scope.feedList[idx];

              $scope.feedList[i] = feedList[i];
              // console.log("feedList" + $scope.feedList);
            }
        }
      }

      );
      // console.log($scope.currentLoadedFeed);
      // console.log($scope.feed_result.feedUrl);
      // if($scope.feed_result.feedUrl == $scope.currentLoadedFeed)
      // {
      //   $scope.feed_result = {};
      // }
      //$scope.feedList.splice(idx, 1);
    };

    $scope.chooseFeed = function(idx) {

     
      $scope.feedList.splice(0, 0, $scope.feedList.splice(idx, 1)[0]); // move to top
      $scope.loadFeed($scope.feedList[0]);
    };

    $scope.layoutDone = function() {
      $timeout(function() { // wait for DOM
        $('a[data-toggle="tooltip"]').tooltip();
      }, 100);
    };

    $scope.loadFeed = function(url, addFeed) {
      //console.log(url);
       if(url){
      rssFeed.get(url).then(function(result) {
        console.log(result);
        if (result.error) {
          $scope.openDialog('test3');
          //alert("ERROR " + result.error.code + ": " + result.error.message + "\nurl: " + url);
          //$scope.setLoading(false);
        }
        else {
          if (addFeed) addFeed();
          var urlParser = document.createElement('a');
          urlParser.href = result.feed.link;
          result.feed.viewAt = urlParser.hostname;
          $scope.feed_result = result.feed;
          if ($scope.feed_result.entries == 0) {
            //$scope.setLoading(false);
          }
        }
      });
    }
    };

    $scope.mediaOne = function(entry) { // return first media object for 'entry'
      return (entry && entry.mediaGroups) ? entry.mediaGroups[0].contents[0] : {url:''};
    };

    $scope.hasVideo = function(entry) {
      var media = $scope.mediaOne(entry);
      return media.type ? (media.type == "video/mp4") : (media.url ? (media.url.indexOf(".mp4") != -1) : false);
    };

}]);