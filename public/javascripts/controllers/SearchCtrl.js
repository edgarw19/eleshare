
app.controller('SearchCtrl', [

'$scope', 'posts', 'auth', 'users', '$http','$window','foundUsers','foundPosts',
function($scope, posts, auth, users, $http, $window,foundUsers,foundPosts){

    // $scope.isLoggedIn = isLoggedIn; 
    $scope.isLoggedIn = auth.isLoggedIn; 
    $scope.posts = []; 
    // $scope.posts = posts.posts; 
    $scope.users = []; 
    $scope.userHasSearched = false; 
   // console.log("I am here: " + foundUsers.data[0].facebook.firstName);
    $scope.usersFound = foundUsers.data; 
    //console.log(foundPosts.data[0]);
    $scope.postsFound = foundPosts.data;

    $scope.authID = auth.currentUserID();

    //console.log($scope.isLoggedIn());
    $scope.incrementUpvotes = function(post) {
      posts.upvote(post); 
    }

    $scope.searchAll = function() {
        $scope.userHasSearched = true; 
        $scope.searchPosts();
        $scope.searchUsers();
        //$window.location.href='/newsfeed#/search';
    }

    $scope.searchPosts = function() {
         posts.getSearch($scope.searchTerm, function(newPosts) {
            $scope.posts = newPosts;

            var allScrapedData = {};    // indexed by post id 
            var completedRequests = 0; 
            // get scraped data for each post
            for (var i = 0; i < $scope.posts.length; i++) {
                // console.log(i, o.posts[i].title); 

                // var id = o.posts[i]._id; 
                // console.log(id); 
                $http.get('/posts/' + $scope.posts[i]._id + '/getScrapedData').success(function(data) {

                    result = {
                        title : data.title,
                        text : data.text.substring(0, 300) + " ...",
                        image : data.image
                    }; 

                    allScrapedData[data.id] = result; 

                    completedRequests++; 
                    // all requests for scraped data completed
                    // we do this bc http req are asynchronous 
                    if (completedRequests == $scope.posts.length) { 
                        for (var j = 0; j < $scope.posts.length; j++) {
                            $scope.posts[j].scrapedData = allScrapedData[$scope.posts[j]._id];
                        }
                    }
                   
                    
                });
         
            }
            
            
        });
       
    }

    $scope.searchUsers = function() {
        users.getSearch($scope.searchTerm, function(newUsers) {
            $scope.users = newUsers; 
      
        });
    
    }

    $scope.isUpvoted = function(post){
            if(post.upvoterIDs.indexOf($scope.authID) == -1)
                return false;
            return true;
        };

    $scope.savePost = function(post){
            console.log($scope.authID+" wants to save post "+post._id);
            users.getUserCB(auth.getCurrentUser._id).success(function(userA){
               $scope.authUser = userA;
               users.addSavedPost($scope.authUser, post);
            });
        };

    users.getUserCB($scope.authID).success(function(userA){
            $scope.authUser = userA;
            $scope.isSaved = function(post){
                console.log(post._id)
                if($scope.authUser.savedPosts.indexOf(post._id) == -1)
                    return false;
                return true;
            }
            });

}]);
