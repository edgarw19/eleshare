app.controller('UserProfileCtrl', [
    '$scope',
    'users',
    'userToFollow',
    'userPosts',
    'savedPosts',
    'auth',
    'posts','user', 'isMyPage', 'isFollowing',
    function($scope, users, userToFollow, userPosts, savedPosts, auth, posts, user,isMyPage, isFollowing) {            
      
        $scope.isLoggedIn = auth.isLoggedIn; 
        $scope.userPosts = userPosts.data; 
        $scope.savedPosts = savedPosts.data;
        $scope.isMyPage = isMyPage;

        var maxChars = 300; 
        for (var i = 0; i < $scope.userPosts.length; i++) {
            $scope.userPosts[i].scrapedData.text = $scope.userPosts[i].scrapedData.text.substring(0, maxChars) + " ...";
        }
        for (var i = 0; i < $scope.savedPosts.length; i++) {
            $scope.savedPosts[i].scrapedData.text = $scope.savedPosts[i].scrapedData.text.substring(0, maxChars) + " ...";
        }

        $scope.userToFollow = userToFollow;
        $scope.userToFollowCategories = userToFollow.data.categories;
        $scope.authID = auth.currentUserID();

        users.getUserCB($scope.authID).success(function(userA){
            $scope.authUser = userA;          
            $scope.isSaved = function(post){
                if($scope.authUser.savedPosts.indexOf(post._id) == -1)
                    return false;
                return true;

            }
            $scope.isFollowing = function(){
                if ($scope.authUser.follows.indexOf($scope.userToFollow.data._id) == -1){
                    return false;
                }
                return true;
            };

        });

        //adds the user whose page the authUser is on to the authUsers follow list
        $scope.followUser = function(){
            users.getUserCB($scope.authID).success(function(userA){
        	   $scope.authUser = userA;
               users.addFollows($scope.userToFollow, $scope.authUser);
            });
        };
        $scope.geturl = function(url)
        {
          $scope.urlReal = url;
        }

        $scope.savePost = function(post){
            users.getUserCB(auth.getCurrentUser._id).success(function(userA){
               $scope.authUser = userA;
               users.addSavedPost($scope.authUser, post);
            });
        };

        $scope.deletePost = function(post, idx){
            posts.deletePost(post);
            $scope.userPosts.splice(idx, 1);
        }

        $scope.incrementUpvotes = function(post) {
            posts.upvote(post); 
        };

        $scope.isUpvoted = function(post){
            if(post.upvoterIDs.indexOf($scope.authID) == -1)
                return false;
            return true;
        };

}]);
