
app.controller('PostsCtrl', [
    '$scope',
    'posts',
    'post',
    'auth', 
    'users',
    function($scope, posts, post, auth, users){
        $scope.isLoggedIn = auth.isLoggedIn; 
        for (var i = 0; i < post.comments.length; i++){
            var index = post.comments[i].upvoterIDs.indexOf(auth.getCurrentUser._id);
            if (index > -1){
                post.comments[i].isUpvoted = true
                }
            else {
                post.comments[i].isUpvoted = false;
            }
            if (post.comments[i].authorID == auth.getCurrentUser._id){
                post.comments[i].isMine = true;
            }
            else {
                post.comments[i].isMine = false;
            }
        }
        $scope.post = post;
        $scope.authID = auth.currentUserID();
        
        users.getUserCB(auth.getCurrentUser._id).success(function(userA){
            $scope.authUser = userA;
            $scope.isSaved = function(){
                if ($scope.authUser.savedPosts.indexOf($scope.post._id) == -1){
                    return false;
                }
                return true;
            };
        });

        posts.getUserPosts(post.authorID, function(relatedPosts){
            $scope.relatedPosts = relatedPosts.splice(0, 4);
            var spliced = false;
            for (var i = 0; i < $scope.relatedPosts.length; i++){
                if ($scope.relatedPosts[i].title == post.title){
                    $scope.relatedPosts.splice(i, 1);
                    spliced = true;
                }
            }
            if (!spliced && $scope.relatedPosts.length > 3){
                $scope.relatedPosts.splice(3, 1);
            }
            
        })

        $scope.deleteComment = function(post, comment){
            posts.deleteComment(post, comment);
        }
        $scope.addComment = function(post) {
            if (post.commentBody === '') { return; }
            if (post.commentBody.length > 2500){
              $scope.commentError = "Comment is over 2500 chars.";
              return;
          }

          var comment = {}; 
          comment.body = post.commentBody; 
          comment.time = new Date().getTime(); 
          comment.timeStr = dateToString(new Date()); 


          posts.addComment(post._id, {
              body: comment,
              author: 'user',
          }).success(function(comment) {
              comment.isMine = true;
              post.comments.push(comment);
          });
          post.commentBody = '';
          $scope.commentError = '';
      }

      $scope.decrementCommentUpvotes = function(post, comment){
          posts.downvoteComment(post, comment);
      };


      $scope.incrementCommentUpvotes = function(post, comment){
          posts.upvoteComment(post, comment);
      };

      $scope.incrementPostUpvotes = function(post) {
        posts.upvote(post); 
    };

    $scope.savePost = function(post){
        users.getUserCB(auth.getCurrentUser._id).success(function(userA){
         $scope.authUser = userA;
         users.addSavedPost($scope.authUser, post);
     });
    };

    $scope.isUpvoted = function(post){
        if(post.upvoterIDs.indexOf($scope.authID) == -1)
            return false;
        return true;
    };

    // $scope.isUpvotedComment = function(comment){
    //     if(comment.upvoterIDs.indexOf($scope.authID) == -1)
    //         return false;
    //     return true
    // }
}
]);