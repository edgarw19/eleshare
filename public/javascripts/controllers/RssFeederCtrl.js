app.controller('RssFeederCtrl',[

  '$scope','$http','$window', 'rssFeed','feedFactory', 'users', 'auth','feedList','user',

  function($scope, $http, $window, rssFeed, feedFactory, users, auth, feedList, user) {
  
 
     $scope.rssFeedsChoice = [
        '//feeds.nytimes.com/nyt/rss/HomePage',
        'http://www.washingtonpost.com/rss/',
        'http://rssfeeds.usatoday.com/usatoday-NewsTopStories',
        'http://www.npr.org/rss/rss.php?id=1001',
'http://feeds.reuters.com/reuters/topNews',
'http://newsrss.bbc.co.uk/rss/newsonline_world_edition/americas/rss.xml'
,'http://feeds.sciencedaily.com/sciencedaily',
'http://www.nasa.gov/rss/image_of_the_day.rss',
          'http://feeds.bbci.co.uk/news/rss.xml',
          'http://feeds.bbci.co.uk/news/business/rss.xml',
          'http://feeds.bbci.co.uk/news/health/rss.xml',
          'http://feeds.bbci.co.uk/news/entertainment_and_arts/rss.xml',
          'http://rss.cnn.com/rss/money_latest.rss',
          'http://rss.cnn.com/rss/cnn_us.rss',
          'http://rss.cnn.com/rss/cnn_latest.rss',
          'http://rss.cnn.com/rss/cnn_topstories.rss',
          'http://feeds.feedburner.com/TechCrunch/startups',
          'http://feeds.feedburner.com/Mobilecrunch',
          'http://feeds.feedburner.com/TechCrunch/social',
          'http://feeds.feedburner.com/TechCrunchIT',
          'http://feeds.feedburner.com/TechCrunch/',
'http://rss.cnn.com/rss/si_topstories.rss',
'http://rss.nytimes.com/services/xml/rss/nyt/Sports.xml',
'http://sports.yahoo.com/top/rss.xml',
'http://www.nba.com/jazz/rss.xml',
'http://www.npr.org/rss/rss.php?id=1008',
'http://www.newyorker.com/feed/humor',
'http://www.npr.org/rss/rss.php?id=13',
'http://www.npr.org/rss/rss.php?id=1045',
'http://www.nationalgeographic.com/adventure/nga.xml', 'http://sports.espn.go.com/espn/rss/news',
'http://sports.espn.go.com/espn/rss/nfl/news',

'http://sports.espn.go.com/espn/rss/nba/news',

'http://sports.espn.go.com/espn/rss/mlb/news',

'http://sports.espn.go.com/espn/rss/nhl/news',

'http://sports.espn.go.com/espn/rss/rpm/news',

'http://soccernet.espn.go.com/rss/news',

'http://sports.espn.go.com/espn/rss/espnu/news',

'http://sports.espn.go.com/espn/rss/ncb/news',

'http://sports.espn.go.com/espn/rss/ncf/news','http://feeds.nationalgeographic.com/ng/photography/photo-of-the-day/'
        ]
        
        //add links without hyphens and also try to use underscore for spaces
         $scope.rssFeedsChoiceNoHttp = [
'New York Times Home Page',
'Washington Post',
'USATODAY.com News Top Stories',
'NPR Topics_News',
'Reuters_Top News',
'BBC News_Americas_World Edition',
          'ScienceDaily Headlines',
          'NASA Image of the Day',
          'bbc_news',
          'bbc_news_business',
          'bbc_news_health',
          'bbc_news_entertainment_and_arts',
          'cnn_money_latest',
          'cnn_us',
          'cnn_latest',
          'cnn_topstories',
          'TechCrunch_startups',
          'TechCrunch_Mobilecrunch',
          'TechCrunch_social',
          'TechCrunch_IT',
          'TechCrunch_latest',
'Sports Illustrated',
'New York Times_Sports',
'Yahoo_Sports_Top News',
'NBA.com_Jazz News',
'NPR Topics - Arts & Culture',
'New Yorker Humor',
'NPR Fresh Air with Terry Gross',
'NPR Topics: Movies',
'National Geographic Adventure', 'ESPN_news',
'ESPN_NFL Headlines',
'ESPN_NBA Headlines',
'ESPN_MLB Headlines',
'ESPN_NHL Headlines',
'ESPN_Motorsports Headlines',
'ESPN_Soccer Headlines',
'ESPN_ESPNU',
'ESPN_College Basketball Headlines',
'ESPN_College Football Headlines', 'National_Geographic_photo_of_the _day'
        ]

    $scope.randomizeArray = function(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
    };

    $scope.feedList = [];
    for(i = 0; i < feedList.data.feeds.length; i++)
    {
      // console.log("feedCategory" + feedList.data.feeds[i].categoryID);
      // console.log(feedList.data.feeds[i].feedLink);
      $scope.feedList.push(feedList.data.feeds[i]);
     
      console.log("feedList" + $scope.feedList);
    }

    $scope.maxCols = 5;
    $scope.rows = [];
    //console.log($scope.feedList.length);
    
    $scope.count = 0;
    $scope.feed_result = [];
    
    $scope.feedList.forEach(function(arrayElement)
      {
        //console.log($scope.count);
        rssFeed.get(arrayElement).then(function(result) {
      //console.log(result);
      if (result.error) {
        alert("ERROR " + result.error.code + ": " + result.error.message + "\nurl: " + feedList[i]);
        //$scope.setLoading(false);
      }
      else {
        //if (addFeed) addFeed();
        // var urlParser = document.createElement('a');
        // urlParser.href = result.feed.link;
        // result.feed.viewAt = urlParser.hostname;
        // //var result_obj = {"link": result.feed.link, "result": result.feed}

        // var resultRandomized = $scope.randomizeArray(result.feed);
        $scope.feed_result.push(result.feed);

        //console.log($scope.feed_result);
      
        $scope.count = $scope.count + 1;
        // console.log($scope.count);
        if ($scope.count == $scope.feedList.length)
        {
          //console.log("I am here1");
          $scope.concat();
        }
      }
    })

    });

    $scope.concat = function(){
      //console.log("I am here");
      $scope.feed_concat = [];
      var maxChars = 200; 
      for (i = 0; i < $scope.feed_result.length; i++)
      {
        //console.log($scope.feed_result[i].entries);
        $scope.feed_concat = $scope.feed_concat.concat($scope.feed_result[i].entries);
      }
      console.log($scope.feed_concat);
      $scope.feed_concatRandomized = $scope.randomizeArray($scope.feed_concat);
      //console.log($scope.feed_concatRandomized);
      var linksArray = $scope.feed_concatRandomized;
      $scope.scrapedData = [];
      $scope.count = 0;
      //console.log($scope.feed_concatRandomized[0]);
      $scope.feed_concatRandomized.forEach(function(arrayElement)
         { 
            //console.log(arrayElement.link);
            var Obj = {"link":arrayElement.link};
            $http({
            url:'/getScrapedData',
            method: "POST",
            data:Obj, 
            headers: {Authorization: 'Bearer ' + auth.getToken()}})
            .success(function(data) 
            {
              //console.log(data.urlList);
              $scope.scrapedData.push( {
                "title": data.title,
                "text": data.text.substring(0, maxChars) + " ...",
                "image" : data.image,
                "link" : arrayElement.link,
                "urlList": data.urlList});
              var matches = [];
              // for(i = 0; i< data.urlList.length; i++)
              // {
              //   //var regexp = new RegExp(arrayElement.link);
              //   // var regexp2 = new RegExp(".jpg");
              //   // var regexp3 = new RegExp(".jpeg");
              //   // console.log(data.urlList[i]);
              //   // console.log(data.image);
              //   // if ((data.urlList[i].match(".jpg") ||
              //   //   data.urlList[i].match(".jpeg")) 
              //   //   && data.urlList[i].match()
              //   //   ){
              //   //   console.log(data.urlList[i]);
              //   //   matches.push(data.urlList[i]);
              //   // }
              //   //   $scope.alternativeUrl = matches[0];
              // }
                
                // $scope.count = $scope.count+ 1;
                // if($scope.count == $scope.feed_concatRandomized.length)
                // {
                //   $scope.fillRows();
                // } 
            }
            );   
        }
      );
      $scope.feedListNoHttp = [];
      for(i = 0; i < $scope.feedList.length; i++)
      {
        for(j = 0; j < $scope.rssFeedsChoice.length;j++)
        {
          if($scope.feedList[i] == $scope.rssFeedsChoice[j])
          {
           
              $scope.feedListNoHttp.push($scope.rssFeedsChoiceNoHttp[j]);
            
          } 
        } 
      }
      
};


// $scope.fillRows = function(){
//   //console.log($scope.feedList);
// for(i = 0 ; i < ($scope.feedList.length)*10/5; i++)
//       {
//         $scope.rows.push([]);
//         for(j = 0; j < $scope.maxCols; j++)
//           {
//           console.log($scope.scrapedData[i*5+j]);
//          // if($scope.feed_concatRandomized[i*5+j].contentSnippet = " ")
//          //  {
//          //    $scope.feed_concatRandomized[i*5+j].contentSnippet
//          //  }
//           $scope.rows[i][j] = $scope.scrapedData[i*5 + j];
//         }
//       }
// };
    $scope.hasContentSnippet = function(col)
    {
        //console.log(col.contentSnippet.length);
       if(col.contentSnippet.length <= 6)
          {
            console.log("I am here");
            return false;
          }
        else return true;
    }


   $scope.loadPage = function(entryObject)
   {
    $window.location.href = entryObject.link;
   };
   $scope.openTab = function(scraped) {
      $scope.url = scraped.link
  }
  

 }]);

