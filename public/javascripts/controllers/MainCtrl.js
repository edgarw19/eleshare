// controller 

var dateToDay = ["Sun", "Mon", "Tues", "Wed", "Thurs", "Fri", "Sat"];
var dateToString = function(date){
  var dateMinutes = "";
  if (date.getMinutes() < 10){
    dateMinutes+="0";
  }
  dateMinutes+=date.getMinutes();
  var timeString = dateToDay[date.getDay()] + " ";
  if (date.getHours() >= 12){
    timeString += (date.getHours()-12) + ":" + dateMinutes +"PM";
  }
  else {
    timeString+=date.getHours()+":"+dateMinutes+"AM";
  }
  return timeString;
}

app.controller('MainCtrl', [
'$scope', 'posts', 'auth', 'users', 'LxNotificationService',
function($scope, posts, auth, users, LxNotificationService){
    $scope.currentUser = auth.getCurrentUser;
    users.getUser(auth.getCurrentUser._id).success(function(data){
      $scope.currentUser.categories = data.categories;
      console.log($scope.currentUser);
    }).error(function(err){});
    $scope.isLoggedIn = auth.isLoggedIn();
    $scope.posts = posts.posts;

    for (var i = 0; i < $scope.posts.length; i++) {
        $scope.posts[i].comments = []; 
        $scope.posts[i].showComments = false; 
        $scope.posts[i].commentsLoaded = false; 
    }

     $scope.categoriesChoices = [
          { name: 'Architecture'},
          { name: 'Art' }, 
          { name: 'Business' },
          { name: 'Education' },
          { name: 'Entertainment' },
          { name: 'Fashion' },
          { name: 'Funny' },
          { name: 'Health' },
          { name: 'History' },
          { name: 'Interesting' },
          { name: 'Life Thoughts' },
          { name: 'News' },
          { name: 'Philosophy' },
          { name: 'Politics' },
          { name: 'Psychology' },
          { name: 'Science' },
          { name: 'Sports' },
          { name: 'Technology' },
          { name: 'Travel' }
        ];
        
    $scope.showShareForm = false; 
    // $scope.selectedCategories = []; 
    $scope.selects = {
      selectedCategories : undefined,
    }

    // $scope.checkedCategories = []; 

    var testComment = {
        body : 'llama llama cat dog', 
        author : 'Joanna Wang', 
        authorID: 123, 
        upvotes : 5, 
        post : {}, 
        upvoterIDs : [], 
        time : 123, 
        timeStr : 'Tues 1:23PM'
    };


    $scope.getComments = function(post) {
        // get post comments if not already loaded 
        if (!post.commentsLoaded) {
            posts.getComments(post, function(comments) {
                for (var i = 0; i < comments.length; i++){
                  var index = comments[i].upvoterIDs.indexOf(auth.getCurrentUser._id);
                  if (index > -1){
                    comments[i].isUpvoted = true
                  }
                  else {
                    comments[i].isUpvoted = false;
                  }
                  if (comments[i].authorID == auth.getCurrentUser._id){
                    comments[i].isMine = true;
                  }
                  else {
                    comments[i].isMine = false;
                  }
                }
                post.comments = comments; 
                setTimeout(function(){
                   post.commentsLoaded = true; 
                }, 500)
            });
            post.showComments = !post.showComments;   
        }
        else {
          post.showComments = !post.showComments;   
        }
    }

    $scope.deleteComment = function(post, comment){
      posts.deleteComment(post, comment);
    }

    $scope.addComment = function(post) {

        console.log(post.commentBody); 
        if (post.commentBody === '') { return; }
        if (post.commentBody.length > 2500){
          $scope.commentError = "Comment is over 2500 chars.";
          return;
        }

        var comment = {}; 
        comment.body = post.commentBody; 
        comment.time = new Date().getTime(); 
        comment.timeStr = dateToString(new Date()); 

        console.log(comment); 

        posts.addComment(post._id, {
          body: comment,
          author: 'user',
        }).success(function(comment) {
          comment.isMine = true;
          post.comments.push(comment);
        });
        post.commentBody = '';
        $scope.commentError = '';
    }

    $scope.loadItems = function(query){
      var regexp = new RegExp(query.toLowerCase());
      var matches = [];
      
      for (category in $scope.categories){
        var obj = {};
        obj.text = $scope.categories[category].toLowerCase();
        if (regexp.test(obj.text)){
          
          matches.push(obj);
        }
      }
      return matches;
    };

    $scope.sort = function(sortKey){
      $scope.settingsMessage = "";
        $scope.posts = posts.posts;
        $scope.posts.sort(function(a, b) {
          var x = a[sortKey]; var y = b[sortKey];
          return ((x > y) ? -1 : ((x < y) ? 1 : 0));
        });
    };

    $scope.sortByTopics = function(){
      console.log("TOPICS");
      var relatedPosts = [];
      for (var entryI = 0; entryI < $scope.posts.length; entryI++){
        for (var userCategoryI = 0; userCategoryI < $scope.currentUser.categories.length; userCategoryI++){
          var userCategoryString = $scope.currentUser.categories[userCategoryI];
          if ($scope.posts[entryI].categories.indexOf(userCategoryString) >= 0){
            relatedPosts.push($scope.posts[entryI]);
            break;
          }
        }
      }
      $scope.posts = relatedPosts;
      var sortKey = 'postDateMillis';
      $scope.posts.sort(function(a, b) {
          var x = a[sortKey]; var y = b[sortKey];
          return ((x > y) ? -1 : ((x < y) ? 1 : 0));
        });
      if ($scope.posts.length == 0){
        $scope.settingsMessage = "Zero posts in your categories!"
      }
    };

    

    $scope.toggleShareForm = function(msg) {
        if ($scope.isLoggedIn) { 
            if (msg == 'clickedFromIcon')       $scope.showShareForm = !$scope.showShareForm;
            else if (msg == 'clickedFromForm')  $scope.showShareForm = true; 
        } else {
            $scope.showShareForm = false; 
        }
    }

    $scope.addPost = function() {
      if (!$scope.link || $scope.link == '' || !$scope.title || $scope.title === '' || !$scope.selects.selectedCategories) {
        $scope.error = "Can't have missing field.";
        return;
        }
      var linkRegExp = new RegExp('^https?','i'); // fragment locator
      if (!linkRegExp.test($scope.link)){
        $scope.error = "Invalid link. Must begin with http.";
        return;
      }
      if ($scope.link.length > 300){
        $scope.error = "Link length is too long";
        return;
      }
      if ($scope.title.length > 300){
        $scope.error = "Description length is too long.";
        return;
      }

      var date = new Date();
      var timeString = dateToString(date);
      var categoriesArray = [];
      // for(i in $scope.tags) {
        // categoriesArray.push($scope.tags[i].text.toLowerCase());
      // }
      for (i in $scope.selects.selectedCategories) {
        categoriesArray.push($scope.selects.selectedCategories[i].name.toLowerCase()); 
      }

      posts.create({
        title: $scope.title,
        link: $scope.link,
        categories: categoriesArray,
        postDateMillis: date.getTime(),
        postDateDay: dateToDay[date.getDay()],
        postDateTimeString: timeString
      });
      $scope.title = ''; 
      $scope.link = ''; 

      $scope.tags = '';
      LxNotificationService.success('Success!');
      $scope.error = "";

    };

    $scope.incrementUpvotes = function(post) {
      posts.upvote(post); 
    }

    $scope.incrementCommentUpvotes = function(post, comment){
      posts.upvoteComment(post, comment);
    };

    $scope.decrementCommentUpvotes = function(post, comment){
      posts.downvoteComment(post, comment);
    };

    $scope.decrementUpvotes = function(post) {
      posts.downvote(post);
      console.log("DOWNVTING MAINCTRL");
    }

    //save Post on the card
    $scope.savePost = function(post){
      users.getUserCB(auth.getCurrentUser._id).success(function(userA){
        $scope.authUser = userA;
        users.addSavedPost($scope.authUser, post);
      });
    };

    $scope.searchPosts = function() {
        posts.getSearch($scope.searchTerm, function(newPosts) {
            $scope.posts = newPosts; 
        });
    }

    $scope.authID = auth.currentUserID();
    
    users.getUserCB($scope.authID).success(function(userA){
      $scope.authUser = userA;
      $scope.isSaved = function(post){

        if($scope.authUser.savedPosts.indexOf(post._id) == -1)
          return false;
        return true;
      }
    });

    $scope.isUpvoted = function(post){
      if(post.upvoterIDs.indexOf($scope.authID) == -1)
        return false;
      return true;
    }

}]);
