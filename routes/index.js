var express = require('express');
var router = express.Router();
var passport = require ('passport'); 
var jwt = require('express-jwt'); 

var mongoose = require('mongoose');
var Post = mongoose.model('Post');
var Comment = mongoose.model('Comment');
var User = mongoose.model('User'); 
var Email = require('../models/Emails.js');

var scraper = require('./scraper');

var https = require('https');




// TODO: use an environment variable for secret; use the same secret as the one in models/User.js
var auth = jwt({secret: 'SECRET', userProperty: 'payload'});

var dateToDay = ["Sun", "Mon", "Tues", "Wed", "Thurs", "Fri", "Sat"];

var dateToString = function(date){
  var dateMinutes = "";
      if (date.getMinutes() < 10){
        dateMinutes+="0";
      }
      dateMinutes+=date.getMinutes();
      var timeString = dateToDay[date.getDay()] + " ";
      if (date.getHours() >= 12){
        timeString += (date.getHours()-12) + ":" + dateMinutes +"PM";
      }
      else {
        timeString+=date.getHours()+":"+dateMinutes+"AM";
      }
    return timeString;
}

// view all posts - GET /posts 
router.get('/posts', function(req, res, next) {
  // returns an array of posts 
  Post.find({}).sort('postDateMillis').limit(40).exec(function(err, posts){
    if(err){ return next(err); }
    res.json(posts);
  });
});

// return all posts whose title contains one of the search terms
router.get('/posts/search/:searchTerm', function(req, res, next) {
  var searchWords = req.params.searchTerm.split(" ");

  var queries = [];

  for (i in searchWords) {
    queries.push({title: new RegExp(searchWords[i], 'i')});
    queries.push({ 'scrapedData.title' : new RegExp(searchWords[i], 'i') });
  }

  Post.find({ '$or' : queries }, function(err, posts){
    if(err){ return next(err); }
    res.json(posts);
  });
});



// return all posts from this user 
router.get('/posts/getUserPosts/:id', function(req, res, next) {
  User.findOne({ '_id' : req.params.id }, function(err, user) {


    if (!user || user.posts.length == 0) res.json([]); 
    
    else {
      var queries = [];

        for (var i = 0; i < user.posts.length; i++) {
          queries.push({ '_id' : user.posts[i] });
        }

        Post.find({ '$or' : queries }, function(err, posts){
          if(err){ return next(err); }
          res.json(posts);
        }); 
    }
  });
});

// return all posts from this user 
router.get('/posts/getSavedPosts/:id', function(req, res, next) {

    User.findOne({ '_id' : req.params.id }, function(err, user) {

        if (user.savedPosts.length == 0) res.json([]); 

        var queries = [];

        // console.log(user.posts); 
        for (var i = 0; i < user.savedPosts.length; i++) {
            // console.log(user.posts[i]); 
            queries.push({ '_id' : user.savedPosts[i] });
        }

        console.log(queries); 

        Post.find({ '$or' : queries }, function(err, savedPosts){
            if(err){ return next(err); }
            res.json(savedPosts);
        }); 

    }); 
    
});

// return all posts in this category 
router.get('/posts/category/:categories', function(req, res, next) {
  // returns an array of posts 
  userCategories = req.params.categories.split('|');

  console.log("getting all posts in categories: " + userCategories); 
  for (i in userCategories)
  {
    userCategories[i] = userCategories[i].toLowerCase();
  }
  
  Post.find({ categories: { $in : userCategories} }, function(err, posts){
    if(err){ return next(err); }
    
    // get scraped data for each post 
    // postsSetCount = 0; 
    // for (i in posts) {
    //     postsSetCount++; 
    //     var url = posts[i].link; 
    //     // posts[i].scrapedData = {}; 
    //     scraper.getData(url, function(data) { 
    //         posts[i].scrapedData = data;
    
    //         // console.log(posts[i].scrapedData); 
    //         if (postsSetCount >= posts.length) {
    //             console.log(posts[0].scrapedData); 
    //             console.log(posts[1].scrapedData); 
    //             res.json(posts); 
    //         }
    //     }); 

    // }
    
    res.json(posts);
  });
  // Post.find({postDateTimeString: "Sat 2:41PM"}, function(err, posts){
  //   // console.log("I am here3");
  //   console.log(posts[0].categories[0]);
  //   if(err){ return next(err); }
  
  //   res.json(posts);
  // });
});

router.post('/emails', function(req, res, next) {
  console.log(req.body);
  var email = new Email(req.body);
  if (!email.email || email.email.length > 50){
    res.json(200);
  }
  else {
  console.log(email);
  email.save(function(err, email){
    res.json(200);
  });
}
});

// add a new post - POST /posts 
router.post('/posts', auth, function(req, res, next) {
  var post = new Post(req.body);
  console.log(req.payload);
  post.author = req.payload.firstName+' '+req.payload.lastName;
  post.authorID = req.payload._id; 

  // save scraped data into post object 
  // TODO: do this upon loading post rather than when saving? 
  scraper.getData(post.link, function(data) {
    post.scrapedData = data; 
    // console.log(post); 
    post.save(function(err, post){
      if(err){ res.send(403); }
        // get the User object associated with the author of the post
        User.findById(post.authorID, function(err, foundUser){
            if(err){ res.send(403); }
            // add the post ID to the Users list of post IDs
            foundUser.addPost(post._id, function(err, updated){
                if(err){ res.send(403); }
                res.send(post);
            });

            // DO NOT REMOVE THIS YET - will use for real-time notifications
            // var notificationText = post.author + ' posted "' + post.title + '" in [';
            // for (var i = 0; i < post.categories.length; i++) {
            //     notificationText += post.categories[i];
            //     if (i < post.categories.length - 1) notificationText += ', ';
            //     else notificationText += ']'
            // }

            // var notification = {
            //     type : 'post',
            //     link : 'newsfeed#/posts/' + post._id,
            //     text : notificationText, 
            //     timeMillis : post.postDateMillis,
            //     timeStr : post.postDateTimeString, 
            //     isRead : false
            // }; 

            // for (var i = 0; i < foundUser.followers.length; i++) {
            //     User.findById(foundUser.followers[i], function(err, foundFollower) {

            //         if (err) { console.log (foundUser.followers[i] + " follower not found"); }
                    
            //         foundFollower.addNotification(notification, function() {
            //             console.log("added notification for follower " + foundFollower._id); 
            //         })

            //     }); 
            // }

        });
      });
  })

});

// for preloading post objects 
router.param('post', function(req, res, next, id) {
  var query = Post.findById(id);

  query.exec(function (err, post){
    if (err) { return next(err); }
    if (!post) { return next(new Error('can\'t find post')); }

    req.post = post;
    return next();
  });
});

// view comments associated with a post - GET /posts/:id 
router.get('/posts/:post', function(req, res) {
  req.post.viewPlus(function(err, post){
    post.populate('comments', function(err, post) {
      if (err) { return next(err); }
      res.json(req.post);
    }); 
  });
});

router.get('/posts/:post/getComments', function(req, res, next) {
    if (req.post.comments.length == 0) res.json([]); 
    
    var queries = []; 

    for (var i = 0; i < req.post.comments.length; i++) {
        queries.push({ '_id' : req.post.comments[i] });
    }

    Comment.find({ '$or' : queries }, function(err, comments){
        if(err){ return next(err); }
        res.json(comments);
    });

});


// get scraped data from a post - title, text, image 
router.get('/posts/:post/getScrapedData', function(req, res, next) {
    //console.log("post:" + req.post); 
    scraper.getData(req.post.link, function(data) {
      data.id = req.post._id; 
      res.json(data); 
    })
  });


// get scraped data from a post - title, text, image 
router.post('/getDetailedData', function(req, res, next) {
    console.log("I am here2");
    console.log(Object.keys(req.body.responseData.results[0]).url);
    //console.log(req.body.results[0].unescapedUrl);
    scraper.getDetailedData(req.body.responseData.results[0].url, function(data) {
      //console.log("Have got all the urls: " + data);
      res.json(data); 
    })
  });

//downvote an upvoted post

// upvote a post - PUT /posts/:id/upvote 
router.put('/posts/:post/downvote', auth, function(req, res, next) {

  upvoter_id = req.payload._id;
//WHEN REWRITING DB, USE THIS CODE, currently, use this instead
  // req.post.downvote(upvoter_id, function(err, post){
  //   console.log("APPARENTLY DELETED DUDE");
  //   if (err) { return next(err); }
  //   res.json(post);
  // });

  var index = req.post.upvoterIDs.indexOf(upvoter_id);
  if (index > -1){
    req.post.upvoterIDs.splice(index, 1);
    req.post.upvotes -= 1;
  }
  req.post.save(function(err, post){
    if (err) {res.send(200);}
    res.json(post);
  });
});



// upvote a post - PUT /posts/:id/upvote 
router.put('/posts/:post/upvote', auth, function(req, res, next) {

  upvoter_id = req.payload._id; 
  upvoter_name = req.payload.firstName + ' '  + req.payload.lastName; 

  // send notification to post author 
  User.findOne({'_id' : req.post.authorID}, function(err, user) {
    if (err) { console.log("user not found"); }

    // check if user already has notification about this post 
    var notificationExists = false; 
    for (var i = 0; i < user.notifications.length; i++) {
      if (user.notifications[i].notification_type == 'upvote' && user.notifications[i].post_id == req.post._id) {
        notificationExists = true; 
        // this commenter is commenting on the post for the first time
        var user_index = user.notifications[i].user_names.indexOf(upvoter_name); 
        if (user_index >= 0) user.notifications[i].user_names.splice(user_index, 1); 
        user.notifications[i].user_names.unshift(upvoter_name); 
        user.notifications[i].isRead = false
        user.notifications[i].timeMillis = new Date().getTime();
        user.notifications[i].timeStr = dateToString(new Date()); 

        break; 
      }
    }

    if (!notificationExists) {
      var newNotification = {
            notification_type : 'upvote',
            post_id : req.post._id,
            post_title : req.post.title,
            user_names : [upvoter_name],
            timeMillis : new Date().getTime(), 
            timeStr : dateToString(new Date()),
            isRead : false
        }; 
      user.notifications.push(newNotification); 
    }

    user.save(function (err, user){
      if(err){ console.log("error saving user notification"); }
    });

  })

  req.post.upvote(req.payload._id, function(err, post){
    if (err) { return next(err); }
    res.json(post);
  });
});

// save a post - PUT /posts/:id/save
router.put('/posts/:post/:authID/save', auth, function(req, res, next) {
  var authUserID = req.params.authID;

  User.findOne({'_id': authUserID}, function(err, thisUser){
    if (err) { return next(err); }
    thisUser.addSavedPost(req.post._id, function(err, post){
      if (err) { return next(err); }
      res.json(post);
    });
  });
});



// delete a post - PUT /posts/:post/delete
router.delete('/posts/:post/delete', auth, function(req, res, next){
  var postID = req.post._id;
  if (!(req.post.authorID === req.payload._id)){
    console.log("NOPE");
    res.send(400);
  }
  else {
    console.log(postID);
    Post.findOne({'_id':postID}).remove().exec();
    res.send(200);
  }
})

router.put('/posts/:post/comments/:comment/downvote', auth, function(req, res, next) {
  downvoter_id = req.payload._id;
  //WHEN WRITING DB, use this code
  //req.comment.downvote instead
  var index = req.comment.upvoterIDs.indexOf(downvoter_id);
  if (index > -1){
    req.comment.upvoterIDs.splice(index, 1);
    req.comment.upvotes-=1;
  }
  req.comment.save(function(err, comment){
    if (err) {res.send(200)};
    res.json(comment);
  });
})


router.delete('/posts/:post/comments/:comment/delete', auth, function(req, res, next){
  var commentID = req.comment._id;
  if (!req.comment.authorID ===(req.payload._id)){
    res.send(400);
  }
  else 
  {
   Comment.findOne({'_id':req.comment._id}).remove().exec(function(err){
      for (var i = 0; i < req.post.comments.length; i++){
        if (req.post.comments[i].equals(commentID)){
          req.post.comments.splice(i,1);
          req.post.upvotes -= 1;
          break;
        }
      }

      req.post.save(function(err, post){
          res.send(200);
      });
    });
  }


})



// add a comment to post by ID - POST /posts/:id/comments 
router.post('/posts/:post/comments', auth, function(req, res, next) {

  var comment = new Comment(req.body.body);
  comment.post = req.post;
  comment.author = req.payload.firstName+' '+req.payload.lastName;
  comment.authorID = req.payload._id; 



  // send notification to post author 
  User.findOne({'_id' : comment.post.authorID}, function(err, user) {
    if (err) { console.log("user not found"); }

    // check if user already has notification about this post 
    var notificationExists = false; 
    for (var i = 0; i < user.notifications.length; i++) {
      if (user.notifications[i].notification_type == 'comment' && user.notifications[i].post_id == comment.post._id) {
        notificationExists = true; 
        // this commenter is commenting on the post for the first time
        var user_index = user.notifications[i].user_names.indexOf(comment.author); 
        if (user_index >= 0) user.notifications[i].user_names.splice(user_index, 1); 
        user.notifications[i].user_names.unshift(comment.author); 
        user.notifications[i].isRead = false
        user.notifications[i].timeMillis = comment.time; 
        user.notifications[i].timeStr = comment.timeStr; 

        break; 
      }
    }

    if (!notificationExists) {
      var newNotification = {
            notification_type : 'comment',
            post_id : comment.post._id,
            post_title : comment.post.title,
            user_names : [comment.author],
            timeMillis : comment.time,
            timeStr : comment.timeStr, 
            isRead : false
        }; 
      user.notifications.push(newNotification); 
    }

    user.save(function (err, user){
      if(err){ console.log("error saving user notification"); }
    });

  })

  comment.save(function(err, comment){
    if(err){ return next(err); }

    req.post.comments.push(comment);
    req.post.save(function(err, post) {
      if(err){ return next(err); }

      res.json(comment);
    });
  });
});

router.get('/posts/:post/comments/:comment', function(req, res, next) {
  res.json(req.comment);
});

// preloading comment objects 
router.param('comment', function(req, res, next, id) {
  var query = Comment.findById(id);

  query.exec(function (err, comment){
    if (err) { return next(err); }
    if (!comment) { return next(new Error('can\'t find comment')); }

    req.comment = comment;
    return next();
  });
});

// upvote a comment - PUT /posts/:id/comments/:id/upvote
router.put('/posts/:post/comments/:comment/downvote', auth, function(req, res, next) {
  downvoter_id = req.payload._id;
  //WHEN WRITING DB, use this code
  //req.comment.downvote instead
  var index = req.comment.upvoterIDs.indexOf(downvoter_id);
  if (index > -1){
    req.comment.upvoterIDs.splice(index, 1);
    req.comment.upvotes-=1;
  }
  req.comment.save(function(err, comment){
    if (err) {res.send(200)};
    res.json(comment);
  });
})

// upvote a comment - PUT /posts/:id/comments/:id/upvote
router.put('/posts/:post/comments/:comment/upvote', auth, function(req, res, next) {

  // var post_title = req.post.title; 
  var post_id = req.post._id; 
  var comment_id = req.comment._id; 
  var comment_body = req.comment.body; 
  var comment_authorID = req.comment.authorID; 
  var upvoter_name = req.payload.firstName + ' '  + req.payload.lastName;

  // send notification to comment author 
  User.findOne({'_id' : comment_authorID}, function(err, user) {
    if (err) { console.log("user not found"); }

    // check if user already has notification about this post 
    var notificationExists = false; 
    for (var i = 0; i < user.notifications.length; i++) {
      if (user.notifications[i].notification_type == 'comment upvote' && user.notifications[i].comment_id == comment_id) {
        notificationExists = true; 
        // this commenter is commenting on the post for the first time
        var user_index = user.notifications[i].user_names.indexOf(upvoter_name); 
        if (user_index >= 0) user.notifications[i].user_names.splice(user_index, 1); 
        user.notifications[i].user_names.unshift(upvoter_name); 
        user.notifications[i].isRead = false;
        user.notifications[i].timeMillis = new Date().getTime();
        user.notifications[i].timeStr = dateToString(new Date()); 
        break; 
      }
    }

    if (!notificationExists) {
      var newNotification = {
            notification_type : 'comment upvote',
            post_id : post_id, 
            comment_id : comment_id,
            comment_body : comment_body,
            user_names : [upvoter_name],
            timeMillis : new Date().getTime(), 
            timeStr : dateToString(new Date()),
            isRead : false
        }; 
      user.notifications.push(newNotification); 
    }

    user.save(function (err, user){
      if(err){ console.log("error saving user notification"); }
    });

  })


  req.comment.upvote(req.payload._id, function(err, comment){
    if (err) { return next(err); }
    res.json(comment);
  });




});

router.get('/login', function(req, res, next){
  res.render('login');
});


router.post('/generateToken', function(req, res, next) {
  console.log("here");
  if(!req.body._id || !req.user){
    return res.status(400).json({message: 'Please fill out all fields'});
  }
  console.log("here");
  console.log("req is", req.body);
  var curUser = new User(req.body);

  res.json({token: curUser.generateJWT()});


});



router.get('/curUser', function(req, res, next) {
  if (req.user){
    res.json(req.user);
  }
  else {
    res.json({});
  }
});

router.get('/users', function(req, res, next) {
  User.find(function(err, users){
    if(err){ return next(err); }

    res.json(users);
  });
});

// router.get('/users/:user/recent', function(req, res, next){
//   User.findOne({'_id': req.user.id}, function(err, user){
//     if (err) {return next(err);}
//     res.json(user.posts);
//   })
// });


router.param('user', function(req, res, next, id) {
  var query = User.findOne({ '_id' : id }); 
  query.exec(function (err, user){
    if (err) { return next(err); }
    if (!user) { return next(new Error('can\'t find user')); }

    req.user = user; 
    return next();
  });
});

router.param('category', function(req, res, next, id) {
});


router.get('/users/:user', function(req, res) {
  User.findOne({'_id': req.user.id}, function(err, user){
    if(err){ return next(err); }
    res.json(user);
  });
});



router.get('/users/:user/categories', function(req, res, next) {
  console.log("I am in categories 1");
  //var user = User.findOne({'_id': id});

  console.log(req.user.username);
  console.log(req.user.categories);
  res.send(req.user.categories); 
}
); 

router.get('/users/:user/notifications', function(req, res, next) {
    res.send(req.user.notifications); 
}); 

router.post('/users/:user/addCategories', auth, function(req, res, next) {
  User.findOne({ '_id' : req.user._id }, function(err, user) {
    var categories = req.body;
    user.addCategory(categories, function(err, updated){
      if(err){ return next(err); }
      res.json(user);
    });


  });   
});

router.get('/users/:user/feedList', function(req, res, next) {
  console.log("I am in categories");
  //var user = User.findOne({'_id': id});

  console.log(req.user.username);
  console.log(req.user.categories);
  res.send(req.user.feeds); 
   }
); 

router.post('/users/:user/addFeed', auth, function(req, res, next) {
  //console.log("I am here");
 
  // console.log(req.user.categories);
  // console.log(req.body.Link.join(""));
   
  User.findOne({ '_id' : req.user._id }, function(err, user) {
    
    var feedLink = req.body.Link.join("");
    user.addFeed(feedLink, function(err, updated){
        if(err){ return next(err); }
        // console.log('user:',user);
        res.json(user.feeds);
    });

}); 
  

});

// get scraped data from a post - title, text, image 
router.post('/getScrapedData', function(req, res, next) {
    
  //console.log(req.body); 
    scraper.getData(req.body.link, function(data) {
        //data.id = req.post._id; 
        res.json(data); 
    })
    
    
});

// get scraped data from a post - title, text, image 
router.get('/getHtmlData', function(req, res, next) {
    
  //console.log(req.body); 
    scraper.getData(req.body.link, function(data) {
        //data.id = req.post._id; 
        res.json(data); 
    })
    
    
});


// follow a user
router.put('/users/:authID/addFollows/:userToAddID', function(req, res, next) {
  var authUserID = req.params.authID;
  var userToAddID = req.params.userToAddID;


  User.findOne({ '_id' : authUserID }, function(err, thisUser) {
    if (err) { return next(err); }
    thisUser.addFollowId(userToAddID, function(err, cb) {
      if (err) { return next(err); }
      User.findOne({'_id' : userToAddID }, function(err, userToAdd){
        if (err) { return next(err); }

        // save notification
        userToAdd.notifications.push( {
            notification_type : 'follower', 
            user_names : [thisUser.facebook.firstName + ' ' + thisUser.facebook.lastName],
            user_id : authUserID, 
            timeMillis : new Date().getTime(), 
            timeStr : dateToString(new Date()),
            isRead : false
        });

        // userToAdd.save(function (err, user){
            // if(err){ console.log("error saving user notification"); }
        // });

        userToAdd.addFollowerId(authUserID, function(err, cb) {
          if (err) { return next(err); }
          res.json(thisUser);
        });
      });
    });
  });
});



router.post('/users/:user/deleteCategories', auth, function(req, res, next) {

  console.log(req.body);
  User.findOne({ '_id' : req.user._id }, function(err, user) {
    
    var categories = req.body;
    
    user.deleteCategory(categories, function(err, updated){
      if(err){ return next(err); }
      res.json(user);
    });

    
  });   
});


router.post('/users/:user/removeFeed', auth, function(req, res, next) {

  
  console.log(req.body.link);
  User.findOne({ '_id' : req.user._id }, function(err, user) {
    
    // var category = req.body.Category;
    var link = req.body.link;
    // console.log(link);
    user.removeFeed(link, function(err, updated){
        if(err){ return next(err); }
        console.log("I am here in the belly");
        console.log(user.feeds);
        res.json(user.feeds);
    });

    
});   
});

router.post('/users/:user/editEmail', auth, function(req, res, next) {
  console.log("I am here");
  console.log(req.body[0].text);
  User.findOne({ '_id' : req.user._id }, function(err, user) {
    
   user.email = req.body[0].text;
   console.log(user.email);
   user.save(function (err){
    if(err){ return next(err); }
    return res.json(user);
  });

   
 });   
});

// return all users whose first/last name contains one of the search terms
router.get('/users/search/:searchTerm', function(req, res, next) {
  var searchWords = req.params.searchTerm.split(" ");

  var queries = [];

  for (i in searchWords) {
    queries.push({ 'facebook.firstName' : new RegExp(searchWords[i], 'i') });
    queries.push({ 'facebook.lastName' : new RegExp(searchWords[i], 'i') });
  }

  User.find({ '$or' : queries }, function(err, posts){
    if(err){ return next(err); }
    res.json(posts);
  });
});

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('login', { title: 'Express' });
});

router.get('/newsfeed', function(req, res, next) {
  console.log("I am in newsfeed");
  res.render('index', { title: 'Express' });
});

router.get('/initialLandingPage', function(req, res, next) {
  res.render('initialLandingPage', { title: 'Express' });
});

router.post('/chromeToken', function(req, res, next) {
  var query = User.findOne({'facebook.email': req.body.email});
  query.exec(function(err, user){
    if (err){return next(err);}
    var key = user.facebook.email+user.facebook.firstName+user.facebook.lastName;
    var origin = req.headers.origin.split(':')[0];
    if (key == req.body.info && origin == 'chrome-extension'){
      console.log("SEND BACK TOKEN");
      res.json({token: user.generateJWT()})
    }
    else {
      console.log("NO TOKEN");
      res.send(400);
    }
  });

});


// =====================================
// FACEBOOK ROUTES =====================
// =====================================
// route for facebook authentication and login
router.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));

// handle the callback after facebook has authenticated the user
router.get('/auth/facebook/callback',
  passport.authenticate('facebook'),function(req,res) {
   // console.log("I am here before req.user");
   // console.log(req.user.facebook);
   // console.log(req.user.facebook.firstTimeLogin);
   // console.log("I am after facebook.firstTimeLogin");
   if(req.user.facebook.firstTimeLogin === 'false'){
    //console.log("I am not in newuser redirect.")
    if(!req.error){
    res.redirect('/newsfeed');
    }
    else
    {
      res.redirect('/#/editprofile');
    }
  }
  
  else
  {
    console.log("I am in newuser redirect!")
    res.render('initialLandingPage');
  }
}
  );

router.get('/logout', function(req, res){
  req.logout();
  res.redirect('/login');
});

// TODO: what do i do with this
function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/'); 
}

module.exports = router;
