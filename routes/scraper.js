var request = require("request");
var cheerio = require("cheerio");
var ImageResolver = require("image-resolver"); 
var fs = require('fs');

var scraper = module.exports = {}; 


var resolver = new ImageResolver();
resolver.register(new ImageResolver.FileExtension());
resolver.register(new ImageResolver.MimeType());
resolver.register(new ImageResolver.Opengraph());
resolver.register(new ImageResolver.Webpage());

scraper.getTitle = function(url, cb) {
    request({ uri: url }, function(error, response, body) {
        if (!error) { 
            var $ = cheerio.load(body); 
            cb($("title").text());
        }
    });
}

scraper.getText = function(url, cb) {
    request({ uri: url }, function(error, response, body) {
        if (!error) {
            var $ = cheerio.load(body); 
            cb($("p").text()); 
        }
    });
}

scraper.getData = function(url, cb) {
    var urlList=[]; 
    request({ uri: url }, function(error, response, body) {
        if (!error) {
            var $ = cheerio.load(body); 
            var result = {}; 
            result.title = $("title").text();
            // TODO: find a better way to get text summary 

            result.text = $("p").text().substring(0,400); 

            // $('li').each(function(i, elem) {
            //     fruits[i] = $(this).text();
            // });

            // resolver.resolve("http://techcrunch.com/2015/04/17/coin-the-one-credit-card-to-rule-them-all-is-finally-shipping/#.xaa3dw:xGno", function(image){ 
            resolver.resolve(url, function(data){
                if (data) {


                    //console.log(url);
                     //console.log("I am here" + data.image);
                    result.image = data.image; 
                } else {
                    //console.log($("img"))
                    console.log( "No image found" );
                }
                cb(result); 
            });

            // result.image = "http://barkpost.com/wp-content/uploads/2015/01/corgi2.jpg";
            // cb(result); 
        }
    });
}

scraper.getDetailedData = function(url,cb){
    var urls = [];
    console.log(url);
    request({ uri: url }, function(error, response, body) {
        if (!error) {
            var $ = cheerio.load(body); 
            //console.log($('*').length);
            var result = {}; 
            var count = 0;
          $('*').each(function(){
            var url = $(this).attr('href');
            //console.log("url: " + url);
            urls.push(url);
            count = count + 1;
          if(count == $('*').length )
           { 
       // console.log(urls);
                 cb(urls);
                    }
          });
          
            // $('li').each(function(i, elem) {
            //     fruits[i] = $(this).text();
            // });
        //console.log("I am in getDetailedData:" + result.title);
            // resolver.resolve("http://techcrunch.com/2015/04/17/coin-the-one-credit-card-to-rule-them-all-is-finally-shipping/#.xaa3dw:xGno", function(image){ 
        

            // result.image = "http://barkpost.com/wp-content/uploads/2015/01/corgi2.jpg";
            // cb(result); 
        }
        else
        {
            cb();
        }
    });
}

scraper.sendData = function(htmlContent,cb)
{
    console.log("I am here in scraper sendData");
    return cb(htmlContent);
}// // testing
// link = "http://techcrunch.com/2015/04/17/coin-the-one-credit-card-to-rule-them-all-is-finally-shipping/#.xaa3dw:xGno";

// scraper.getTitle(link, function(title) {
//     console.log(title); 
// });

// scraper.getText(link, function(text) {
//     console.log(text); 
// });
