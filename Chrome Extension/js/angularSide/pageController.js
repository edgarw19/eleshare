
myApp.service('pageInfoService', function() {
  this.getInfo = function(callback) {
    var model = {};

    chrome.tabs.query({'active': true},
      function (tabs) {
        if (tabs.length > 0)
        {
          model.title = tabs[0].title;
          model.url = tabs[0].url;

          chrome.tabs.sendMessage(tabs[0].id, { 'action': 'PageInfo' }, function (response) {
            model.pageInfos = response;
            callback(model);
          });
        }

      });
  };
});

var FacebookAPI = function(accessToken) {
  this.request = function(method, arguments, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
      callback(JSON.parse(xhr.response));
    };

    xhr.open("GET", "https://graph.facebook.com/v2.2/" + method + "?access_token=" + accessToken+"?fields=email");
    xhr.send();
  };
}

var fixedCategories = [
{ name: 'Architecture'},
{ name: 'Art' }, 
{ name: 'Business' },
{ name: 'Education' },
{ name: 'Entertainment' },
{ name: 'Fashion' },
{ name: 'Funny' },
{ name: 'Health' },
{ name: 'History' },
{ name: 'Interesting' },
{ name: 'Life Thoughts' },
{ name: 'News' },
{ name: 'Philosophy' },
{ name: 'Politics' },
{ name: 'Science' },
{ name: 'Sports' },
{ name: 'Technology' },
{ name: 'Travel' }
];
var dateToDay = ["Sun", "Mon", "Tues", "Wed", "Thurs", "Fri", "Sat"];
var dateToString = function(date){
  var dateMinutes = "";
  if (date.getMinutes() < 10){
    dateMinutes+="0";
  }
  dateMinutes+=date.getMinutes();
  var timeString = dateToDay[date.getDay()] + " ";
  if (date.getHours() >= 12){
    timeString += (date.getHours()-12) + ":" + dateMinutes +"PM";
  }
  else {
    timeString+=date.getHours()+":"+dateMinutes+"AM";
  }
  return timeString;
}

myApp.controller("PageController", ['$scope', '$http', 'auth', function ($scope, $http, auth) {
  auth.isLoggedIn(function(answer, user){
    $scope.isLoggedIn = answer;
    if (answer){
      console.log("IS LOGGED IN IS" + $scope.isLoggedIn);
    $scope.username = user.username;
    }

     $scope.$apply();
  });
$scope.isShared = false;
$scope.loadItems = function(query){
  var regexp = new RegExp(query.toLowerCase());
  var matches = [];

  for (category in fixedCategories){
    var obj = {};
    obj.text = fixedCategories[category].name.toLowerCase();
    if (regexp.test(obj.text)){        
      matches.push(obj);
    }
  }
  return matches;
};

$scope.login = function(){
  var redirectUrl = "https://baaenjnmlaejajnalldcecpdafeggelb.chromiumapp.org/lolz";
  var clientId = "686419118147105";
  var redirectUri = 'https://'+chrome.runtime.id+'.chromiumapp.org/provider_cb';
  var access_token = null;
  var authUrl = "https://graph.facebook.com/oauth/authorize?" +
  "client_id=" + clientId + "&" +
  "response_type=token&" + "scope=email&"+
  "redirect_uri=" + encodeURIComponent(redirectUrl);
  chrome.identity.launchWebAuthFlow({url: authUrl, interactive: true},
    function(responseUrl) {
     if (chrome.runtime.lastError) {
       console.log(chrome.runtime.lastError);
       return;
     }     
     console.log("SUCCESS LOGIN");   
     var accessToken = responseUrl.substring(responseUrl.indexOf("=") + 1);
     var api = new FacebookAPI(accessToken);
     api.request("me", undefined, function(data) {  
        console.log(data);
      var eleshareUser = {};
      eleshareUser.email = data.email;
      eleshareUser.token = "hi";
      eleshareUser.info = data.email+data.first_name+data.last_name;
      $http.post('http://www.elefeed.com/chromeToken', eleshareUser).
      success(function(res){
        $scope.loginError = "";
        auth.saveToken(res.token);
        auth.isLoggedIn(function(answer, user){
          $scope.isLoggedIn = answer;
          $scope.username = user.username;
          $scope.$apply();
        });
      }).error(function(err){
        $scope.loginError = "Please make an account at elefeed.com first."
      });
    });
   });
};
chrome.tabs.query({'active': true, 'lastFocusedWindow': true}, function(tabs){
  var url = tabs[0].url;
  $scope.url = url; 
  $scope.title = tabs[0].title;
  $scope.$apply();   
});
$scope.share = function(){
  console.log($scope.categories);
  console.log($scope.description);
  console.log($scope.url);
  console.log($scope.title);
  var error = "";
  if (!$scope.description) error = "Please add a description.";
  if ($scope.description){
    if (!$scope.description.length > 200) error = "Max description is 200 chars.";
  }
  if ($scope.categories.length == 0) error = "Please add a category";
  if (error.length > 0){
    $scope.error = error;
    return;
  } 
  var date = new Date();
  var timeString = dateToString(date);
  var categoriesArray = [];
  for (i in $scope.categories){
    categoriesArray.push($scope.categories[i].text);
  }
  var post = {
    title: $scope.description,
    link: $scope.url,
    categories: categoriesArray,
    postDateMillis: date.getTime(),
    postDateDay: dateToDay[date.getDay()],
    postDateTimeString: timeString
  };
  console.log(post);

  $http.post('http://www.elefeed.com/posts', post, {
    headers: {Authorization: 'Bearer ' + auth.token}
  }).success(function(data){
    console.log("Posted!");
    window.location.href="done.html";
  }).error(function(){
    console.log("Couldn't post!");
  });
  $scope.error = "";
  $scope.categories = [];
  $scope.description = "";
  $scope.isShared = true;
}
}]);