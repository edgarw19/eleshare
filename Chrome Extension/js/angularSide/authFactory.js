myApp.factory('auth', ['$http', '$window', function($q, $http, $window){
  var auth = {};
  auth.token = "";

  auth.decode = function(s){
    var e={},i,b=0,c,x,l=0,a,r='',w=String.fromCharCode,L=s.length;
    var A="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    for(i=0;i<64;i++){e[A.charAt(i)]=i;}
    for(x=0;x<L;x++){
        c=e[s.charAt(x)];b=(b<<6)+c;l+=6;
        while(l>=8){((a=(b>>>(l-=8))&0xff)||(x<(L-2)))&&(r+=w(a));}
    }
    return r;
  }

  auth.saveToken = function (token){
    console.log("TOKENS saved DUDE");
    chrome.storage.local.set({key: token}, function(){
      auth.getToken(function(){});
    });
  };

  auth.getToken = function (cb){
    chrome.storage.local.get("key", function(obj){
      auth.token = obj.key;
      console.log("AUTH TOKEN IS" + auth.token);
      cb(obj.key);
    });
  };

  auth.isLoggedIn = function(cb){
    auth.getToken(function(token){
      if (token){
        console.log("is logged in dude");
        // decode encoding 
        var payload = JSON.parse(auth.decode(token.split('.')[1]));
        cb(true, payload);
      }
      else {
         console.log("isn't logged in");
        cb(false, {});
      }
    })
    };


    auth.currentUser = function(){
      if (auth.isLoggedIn(function(){})){
        var token = auth.token;
        var payload = JSON.parse(auth.decode(token.split('.')[1]));
        return payload;
      }
    }


    auth.currentUserID = function(){
      if (auth.isLoggedIn(function(){})){
        var token = auth.token;
        var payload = JSON.parse(auth.decode(token.split('.')[1]));
        console.log(payload); 
        return payload._id;
      }

    };


    auth.logOut = function(){
      console.log("logout dude");
      chrome.storage.local.remove("key", function(){});
    };

    return auth;

  }])

