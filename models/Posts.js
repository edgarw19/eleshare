var mongoose = require('mongoose');

var PostSchema = new mongoose.Schema({
  title: String,
  link: String,
  author: String, 
  authorID: String,
  description: String, 
  upvotes: {type: Number, default: 0},
  views: {type: Number, default: 0},
  comments: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }],
  categories: [String],
  postDateMillis: Number,
  postDateDay: String,
  postDateTimeString: String,
  upvoterIDs: [String],
  scrapedData: {
    title: String,
    text: String,
    image: String
  }
});

PostSchema.methods.upvote = function(userID, callBack) {
  // only increment upvote if the new ID is not already in the upvoter_ids array
  this.upvotes += 1;
  this.upvoterIDs.push(userID);
  this.save(callBack);
};

// PostSchema.methods.downvote = function(userID, callBack) {
//   // only increment upvote if the new ID is not already in the upvoter_ids array
//   var index = this.upvoterIDs.indexOf(userID);
//   if (index > -1){
//     this.upvoterIDs.splice(index, 1);
//     this.upvotes -= 1;
//   }
//   this.save(callback);
// };

PostSchema.methods.viewPlus = function(callBack) {
  // only increment upvote if the new ID is not already in the upvoter_ids array
  this.views += 1;
  this.save(callBack);
};

PostSchema.methods.addCategory = function(category, callBack) {
    this.categories.push(category); 
    this.save(callBack);
}

var Post = mongoose.model('Post', PostSchema);
// Post.remove({}, function(){});

