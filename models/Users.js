var mongoose = require('mongoose'); 
var crypto = require('crypto'); 
var jwt = require('jsonwebtoken'); 


var UserSchema = new mongoose.Schema({
    username: {type: String, lowercase: true, unique: true}, 
    hash: String, 
    salt: String,
    categories: [String],
    feeds:[String],
    posts: [String], //posts is an array of post IDs this user posted
    savedPosts: [String], //posts saved for later reading, by ID
    follows: [String], //users I follow (by ._id)
    followers: [String], //users who follow me (by ._id)
    recentPosts: [String],
    email: String,


    facebook : {
        id          :  String, 
        token       :  String, 
        email       :  String,
        firstName   :  String,
        lastName    :  String, 
        firstTimeLogin: String, 
    },

    notifications : [{
        notification_type : String, // comment, upvote, follower 
        // link        : String

        // for comment + upvote notifications 
        text         : String, 
        post_id      : String, 
        post_title   : String, 
        comment_id   : String, 
        comment_body : String, 
        user_names   : [String],
        // for follower notifications 
        num_users    : Number, 
        user_id      : String, 
        // for all notifications 
        timeMillis   : Number, 
        timeStr      : String,
        isRead       : Boolean
    }]

});

UserSchema.methods.setPassword = function(password) {
    this.salt = crypto.randomBytes(16).toString('hex'); 
    // password, salt, iterations, key length 
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex'); 
};

UserSchema.methods.validPassword = function(password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64).toString('hex'); 
    return this.hash === hash; 
};

UserSchema.methods.generateJWT = function() {
    // expiration 60 days 
    var today = new Date(); 
    var exp = new Date(today); 
    exp.setDate(today.getDate() + 60); 

    // TODO: use an environment variable for referencing the secret
    return jwt.sign({
        _id: this._id, 
        username: this.username, 
        firstName: this.facebook.firstName,
        lastName: this.facebook.lastName,
        exp: parseInt(exp.getTime() / 1000),
    }, 'SECRET');

};

UserSchema.methods.addCategory = function(categories, cb) {
    //console.log("categories:" + categories);
    for (category in categories) {
        var newCategory = categories[category];
        console.log(categories[category]);
        console.log(this.categories);
        var index  = this.categories.indexOf(categories[category]);
        if(index == -1)
        {
            
            this.categories.push(newCategory); 
        }
    }
    this.save(cb);
};

UserSchema.methods.addFeed = function(feed,cb) {
    var index = this.feeds.indexOf(feed);
    if(index == -1 && this.feeds.length < 5)
     {
        console.log(this.feeds.length);
        this.feeds.push(feed); 
    }
    found = true;
    this.save(cb);
};

UserSchema.methods.removeFeed = function(link,cb) {

    var index = this.feeds.indexOf(link);
    if(index != -1)
    {
        console.log("Index: " + index);
        console.log("I am here in removeFeed: " + this.feeds[index]);
        this.feeds.remove(this.feeds[index]);  
    }
    this.save(cb);
};

UserSchema.methods.deleteCategory = function(categories, cb) {
    
    // console.log("categoriesToBeDeleted:" + categories);
    for (var category in categories) {
        // console.log(categories[category].value);
       if(categories[category].value){
        // console.log("to be deleted1: " + categories[category].category );
        // console.log("current categories:" + this.categories);
        var index = this.categories.indexOf(categories[category].category);
        // console.log(index);
        this.categories.remove(this.categories[index]); 
       }   
    }
    // console.log("newCategories:" + this.categories);
    this.save(cb);
};

//add a post ID to the array of posts belonging to this user (as an author)
UserSchema.methods.addPost = function(postID, cb){
    this.posts.push(postID);
    this.save(cb);
}


UserSchema.methods.addNotification = function(notification, cb) {
    this.notifications.push(notification); 
    this.save(cb); 
}
//add a post ID to the array of savedPosts belonging to this user (as an author)
UserSchema.methods.addSavedPost = function(postID, cb){
    if(this.savedPosts.indexOf(postID) == -1){
        this.savedPosts.push(postID);
    }
    this.save(cb);
}

//remove a post ID to the array of savedPosts belonging to this user (as an author)
UserSchema.methods.removeSavedPost = function(postID, cb){
    if(this.savedPosts.indexOf(postID) != -1){
        console.log("I am here in removeSavedPost, users model" + postID);
        this.savedPosts.remove(postID);
    }
    this.save(cb);
}

UserSchema.methods.addFollowId = function(userID, cb) {
  // only increment upvote if the new ID is not already in the upvoter_ids array
    if(this.follows.indexOf(userID) == -1){
        this.follows.push(userID);
    }
    this.save(cb);
};

UserSchema.methods.addFollowerId = function(userID, cb) {
  // only increment upvote if the new ID is not already in the upvoter_ids array
    if(this.followers.indexOf(userID) == -1){
        this.followers.push(userID);
    }
    this.save(cb);
};

var User = mongoose.model('User', UserSchema); 

