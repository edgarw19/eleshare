var mongoose = require('mongoose');

var CommentSchema = new mongoose.Schema({
  body: String,
  author: String,
  authorID: String,
  upvotes: {type: Number, default: 0},
  post: { type: mongoose.Schema.Types.ObjectId, ref: 'Post' },
  upvoterIDs: [String],
  time: Number, 
  timeStr: String
});

CommentSchema.methods.upvote = function(userID, callBack) {
  this.upvotes += 1;
  this.upvoterIDs.push(userID);
  this.save(callBack);
};

CommentSchema.methods.downvote = function(userID, callback){
  var index = this.upvoterIDs.indexOf(userID);
  if (index > -1){
    this.upvoterIDs.splice(index, 1);
    this.upvotes-=1;
  }
  this.save(cb);
}

mongoose.model('Comment', CommentSchema);