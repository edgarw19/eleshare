
var running_locally = true; 


if (running_locally) {
    module.exports = {
        'facebookAuth' : {
            'clientID'      : '923658037665192', 
            'clientSecret'  : '27f7c50ef3dc9a565624286350981795', 
            'callbackURL'   : 'http://localhost:3000/auth/facebook/callback'
        },
    };
} else {
    module.exports = {
         'facebookAuth' : {
            'clientID'      : '686419118147105', 
            'clientSecret'  : 'c619b41793b2e1de02ece8fb7d059ab0', 
            'callbackURL'   : 'http://elefeed.com/auth/facebook/callback'
        },
    };
}
