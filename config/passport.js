var passport = require('passport'); 
var LocalStrategy = require('passport-local').Strategy; 
var FacebookStrategy = require('passport-facebook').Strategy; 
var facebook = require('./facebook.js');
var mongoose = require('mongoose'); 
var User = mongoose.model('User'); 
var configAuth = require('./auth'); 

// how to authenticate a user given a username and a password 
passport.use(new LocalStrategy(
  function(username, password, done) {
    User.findOne({ username: username }, function(err, user) {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, { message: 'Incorrect username!' }); 
      }
      if (!user.validPassword(password)) {
        return done(null, false, { message: 'Incorrect password!' });
      }
      return done(null, user); 
    });
  }
));

passport.serializeUser(function(user, done) {
  console.log("fb serialize user"); 
  done(null, user._id);
});

passport.deserializeUser(function(id, done) {
  console.log("fb deserialize user");
  User.findById(id, function(err, user){
    done(err, user);
  })
});

// =========================================================================
// FACEBOOK ================================================================
// =========================================================================
passport.use(new FacebookStrategy({

        // pull in our app id and secret from our auth.js file
        clientID        : configAuth.facebookAuth.clientID,
        clientSecret    : configAuth.facebookAuth.clientSecret,
        callbackURL     : configAuth.facebookAuth.callbackURL

    },

    // facebook will send back the token and profile
    function(token, refreshToken, profile, done) {

        console.log("FACEBOOK PROFILE: ----\n", profile); 
        console.log(profile._json.email);

        User.findOne({ 'facebook.email' : profile._json.email }, function(err, user) {
          //console.log("WHAAT");
          if (err) { console.log("NO USER");return done(err); }
          if (user) {

            //console.log("FOUND USER");
            user.facebook.firstTimeLogin = "false";
            
            return done(null, user); 
          } else {
            //console.log("new user"); 

            var newUser = new User(); 

            // TODO: should we set username to this? 
            newUser.username            = profile.name.givenName; 
            newUser.facebook.id         = profile.id; 
            newUser.facebook.token      = profile.token; 
            newUser.facebook.firstName  = profile.name.givenName; 
            newUser.facebook.lastName   = profile.name.familyName; 
            newUser.facebook.email      = profile._json.email; 
            newUser.email = profile._json.email;
            newUser.facebook.firstTimeLogin = true;

            facebook.getFbData(token, '/me/friends', function(data){
              //console.log("I am here in friends.");
              //console.log(data);
            });

           
            

            newUser.save(function(err) {
              if (err) throw err; 
              return done(null, newUser)
            });

          }
        });
        }
    )
);

module.exports = function(passport) {




}